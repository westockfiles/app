import React from "react"
import InputField from "../Misc/InputField"
import { SafeAreaView, StyleSheet, Text} from "react-native"
import CustomTextButton from "../Misc/CustomTextButton"
import Pressable from "react-native/Libraries/Components/Pressable/Pressable"

/**
 * Class LoginForm
 * props :
 */
class LoginForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            email : "",
            password : "",
            state_email : "",
            state_password : ""
        };
    
        /**
         * inputHandler, function triggered on input change event
         * @param {*} name 
         * @param {*} event 
         */
        this.inputHandler = (name, event)=>{
            this.setState({[name] : event.nativeEvent.text});
        };
    
        /**
         * buttonHandler, function triggered on press event
         * @param {*} event 
         */
        this.buttonHandler = (event)=>{
            const fake_bdd = require("../../fake_bdd/bdd.json");
            const is_email_correct = fake_bdd.email == this.state.email;
            const is_password_correct = fake_bdd.password == this.state.password;
            if(is_email_correct && is_password_correct){
                this.setState({state_email : "valid", state_password : "valid"});
                this.props.navigation.navigate("HomeNavigator", {screen:"Home"});
            }else{
                if(is_email_correct){
                    this.setState({state_email : "valid", state_password : "cross"});
                }else{
                    this.setState({state_email : "cross", state_password : "cross"});
                }
            }
        };
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <SafeAreaView style={styles.body}>
                    <Text style={styles.title}>Connexion à votre compte</Text>
                    <SafeAreaView style={styles.fields}>
                        <InputField name="email" icon="letter" rounded={true} state={this.state.state_email} value={this.state.email} label="Email" inputHandler={this.inputHandler.bind(this)} focusHandler={(event)=>{}} secure={false} canShow={false}></InputField>
                        <InputField name="password" icon="lock" rounded={true} state={this.state.state_password} value={this.state.password} label="Mot de passe" inputHandler={this.inputHandler.bind(this)} focusHandler={(event)=>{}} secure={true} canShow={false}></InputField>
                        <CustomTextButton title="Se connecter" invert={true} padTop={15} padBot={15} handler={this.buttonHandler.bind(this)}></CustomTextButton>
                    </SafeAreaView>
                    <Pressable onPress={()=> this.props.navigation.navigate('NewPassForm')}>
                        <Text style={styles.mdp_f}>Mot de passe oublié ?</Text>
                    </Pressable>
                </SafeAreaView>
                <SafeAreaView style={styles.spacer}></SafeAreaView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    body:{
        flex:1,
        paddingLeft : 25,
        paddingRight : 25,
        alignSelf : "center",
        justifyContent:"center",
    },
    fields:{
        flex:0.40,
        justifyContent : "space-between",
        //backgroundColor : "red"
    },
    title:{
        fontWeight : "bold",
        color : "#000",
        fontSize : 20,
        marginBottom : 30
    },
    mdp_f:{
        paddingTop : 25,
        alignSelf : "center",
        fontSize : 13,
        color:"#1590ff"
    },
    spacer:{
        flex:0.20
    }
})

export default LoginForm;