import React from 'react';

import { SafeAreaView, StyleSheet, Text,FlatList,Alert, ImageBackground, ScrollView, Modal, Keyboard } from "react-native"
import profile from '../../src/imgs/profile1.jpg'
import Pencil from '../../src/icons/pencil.svg'
import Signature from '../../src/imgs/signature.png';
import CustomIconButton from "../Misc/CustomIconButton"
import CustomTextButton from "../Misc/CustomTextButton"
import FlashMessage from "react-native-flash-message";
import { showMessage, hideMessage } from "react-native-flash-message";
import Card from '../Misc/Card';
import Popup from '../Misc/Popups/Popup';
import RSquareProfileList from '../Misc/RoundedSquare/RSquareList';
import profiles from '../../src/imgs/profiles';
import DrawSignature from '../Misc/DrawSignature';


/* TODO
Quand je click sur le button d'édition, changer la couleur du svg et pas du background

*/


class Parameters extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            display_popup : -1,

            profiles : [
                "profile1",
                "profile2"
            ],

            current_profile_img : "profile2",
        }

        this.user_profiles_list = [
            "profile1",
            "profile2",
            "profile3"
        ]

        this.cards = [
            {
                title : "Gestion des comptes",
                fields : [
                    {
                        name : "profiles",
                        profiles : this.state.profiles,
                        deleteSubProfileHandler : (e) =>{
        
                        },
                        handlerAddUser : (e) =>{
                            this.setState({
                                display_popup : 0,
                            })
                        }
                    }
                ]

            },
            {
                title : "Notifications",
                fields : [
                    {
                        name : "notifications",
                        radioButtons : [
                            {
                                id : 0,
                                label : "Aucune notification"
                            },
                            {
                                id : 1,
                                label : "Seulement pour les deadlines"
                            }
                        ],
                        changeHandler : (index)=>{}
                    }
                ]
            },
            {
                title : "Signature",
                fields : [
                    {
                        name : "signature",
                        source : Signature,
                        handlerChangeSignature : (e) =>{
        
                        }
                    }
                ]
            }
        ]

        this.popups = [
            {
                closeHandler : ()=>{
                
                    Keyboard.dismiss();
                    this.setState({display_popup : -1});

                },
                title : "Ajouter un compte",
                fields : [
                    {
                        name : "input",
                        input_name : "mail_username",
                        input_icon : "",
                        input_value : "",
                        input_opt_icon : {
                            name : "plus",
                            color : "#1599ff",
                            width : 20,
                            height : 20,
                            pressHandler:(event)=>{
                                const new_profiles = this.popups[0].fields[0].input_value.split(",");
                                new_profiles.forEach(new_profile=>{
                                    if(this.popups[0].fields[1].profiles_profiles.find(profile=>{return profile == new_profile.toLowerCase().trim()}) === undefined){
                                        this.popups[0].fields[1].profiles_profiles.push(new_profile.toLowerCase().trim());
                                    }
                                });
                                this.popups[0].fields[0].input_value = "";
                                this.setState({});
                            }
                        },
                        input_rounded : false,
                        input_label : "Identifiant ou Email",
                        input_secure : false,
                        input_can_show : false,
                        inputHandler : (name, event)=>{
                            this.popups[0].fields[0].input_value = event.nativeEvent.text;
                            this.setState({})
                        },
                        focusHandler : (event)=>{

                        }
                    },
                    {
                        name : "profiles",
                        profiles_title : "Comptes ajoutés :",
                        profiles_profiles : [...this.state.profiles],
                        profiles_can_delete : true,
                        deleteHandler : (evt, index)=>{
                            this.popups[0].fields[1].profiles_profiles.splice(index, 1);
                            this.setState({});
                        }
                    }
                ],
                button_title : "Valider",
                pressHandler:(evt)=>{
                    this.popups[0].fields[0].input_opt_icon.pressHandler({});
                    Keyboard.dismiss();
                    this.cards[0].fields[0].profiles = this.popups[0].fields[1].profiles_profiles;
                    this.setState({profiles : this.popups[0].fields[1].profiles_profiles, display_popup : -1});
                }
            },
            {
                closeHandler : ()=>{
                
                    Keyboard.dismiss();
                    this.setState({display_popup : -1});

                },

                title : "Modifier votre image de profil",

                fields : [
                    {
                        name: "chooseProfileImg",
                        user_profiles_list : this.user_profiles_list,
                        current_profile_img : this.state.current_profile_img,
                        imgProfileHandler : (e, src) => {
                            this.popups[1].fields[0].current_profile_img = src;
                            this.setState({});
                        }
                    }
                ],

                button_title : "Valider",

                pressHandler:(evt)=>{
                    this.setState({current_profile_img : this.popups[1].fields[0].current_profile_img, display_popup : -1});
                }
            }
        ]

    }

    editProfileHandler = (e) => {
        this.setState({display_popup : 1})
    }


    deconnexionHandler = (e) =>{
        this.props.navigation.navigate("FormsNavigator", {screen:"LoginForm"});
    }
    
   
    render(){
      
        return(
            <SafeAreaView>

                <Modal animationType="fade" transparent={true} visible={this.state.display_popup !=-1}>
                    <Popup popup={this.popups[this.state.display_popup]}></Popup>
                </Modal>

                <ScrollView>
                    <SafeAreaView style={styles.container}>

                        <SafeAreaView style={styles.userProfileContainer}>
                            
                            <SafeAreaView style={styles.userProfileImgEdit}>
                                <SafeAreaView style={styles.userProfileImgContainer}>
                                    <ImageBackground  style={styles.userProfileImg} source={profiles[this.state.current_profile_img]}/>
                                </SafeAreaView>
                                <SafeAreaView style={styles.edit}>
                                    <CustomIconButton press={this.editProfileHandler} Icon={Pencil} width={12} height={12} padding={12} shadow={true} pressOut={{color : "#acacac", backgroundColor : "#ffffff"}} pressIn={{color : "#1590FF", backgroundColor : "#ffffff"}}></CustomIconButton>
                                </SafeAreaView>
                            </SafeAreaView>

                            <Text style={styles.userProfileName}>@enzo.rodriguesdr</Text>
                        
                        </SafeAreaView>
                        
                        {
                            this.cards.map((card, id) => {
                                return(
                                    <Card key={id} card={card}></Card>
                                )
                            })
                        }


                        <DrawSignature></DrawSignature>

                        <CustomTextButton handler={this.deconnexionHandler.bind(this)} style={styles.deconnexion} padTop={12} padBot={12} color={"red"} title="Déconnexion"></CustomTextButton>


                    </SafeAreaView>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding: 20,
        flexDirection: 'column',
        alignItems: 'center'
    },

    userProfileContainer:{
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 40,

    },

    userProfileImgEdit:{
        margin: 10,
        width: '40%',
        aspectRatio: 1,
        borderRadius: 30,
        
        shadowColor: "#000",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 5,
    },

    userProfileImgContainer:{
        borderRadius: 30,
        flex: 1,
        overflow: 'hidden',
    },

    userProfileImg:{
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },

    userProfileName:{
        fontSize: 18,
    },

    edit:{
        position : "absolute",
        alignSelf : 'flex-end',
        bottom : -6,
        right : -6,

    },

    deconnexion:{
        width:'100%',
    }
   
})

export default Parameters;