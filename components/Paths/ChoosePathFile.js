import React, { Fragment } from "react";

import { SafeAreaView, StyleSheet, Alert, ScrollView } from "react-native";

import folderTree from "../Misc/folderTree";

import FlashMessage from "react-native-flash-message";

import Card from "../Misc/Card";

class ChoosePathFile extends React.Component {
	constructor(props) {
		super(props);
		this.cards = [
			{
				title: "Chemin automatique",
				fields: [
					{
						name: "path",
						path: "Perso / études / ",
						file_name: "diplôme.pdf",
					},
					{
						name: "button",
						textButton: "Valider ce chemin",
					},
				],
			},
			{
				title: "Chemin dossier courant",
				fields: [
					{
						name: "path",
						path: "Perso / automobile / voiture / ",
						file_name: "diplôme.pdf",
					},
					{
						name: "button",
						textButton: "Valider ce chemin",
					},
				],
			},
			{
				title: "Chemin manuel",
				fields: [
					{
						name: "tree",
						data_tree: [folderTree],
					},
					{
						name: "button",
						textButton: "Valider ce chemin",
					},
				],
			},
		];
	}

	render() {
		return (
			<ScrollView>
				<SafeAreaView style={styles.container}>
					{this.cards.map((card, id) => {
						return <Card key={id} card={card}></Card>;
					})}
					<FlashMessage position="top" />
				</SafeAreaView>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20,
	},
});

export default ChoosePathFile;
