import React, { Fragment } from "react";

import {
	SafeAreaView,
	StyleSheet,
	Text,
} from "react-native";

import TreeView from "./TreeView";
import LittleFolder from "../../src/icons/littleFolder.svg";
import RightArrow from "../../src/icons/rightArrow.svg";
import DownArrow from "../../src/icons/downArrow.svg";
import LineArrow from "../../src/icons/lineArrow.svg";


class PathComp extends React.Component {

	constructor(props){
		super(props);
		
	}


	//Used by the Flatlist component to render the list of deadlines

	getIndicator = (isExpanded, hasChildrenNodes, level) => {
		if (!hasChildrenNodes) {
			return (
				<LineArrow
					style={{
						marginLeft: 25 * level,
						alignSelf: "center",
					}}
					width={20}
					height={20}
				></LineArrow>
			);
		} else if (isExpanded) {
			return (
				<DownArrow
					style={{
						marginLeft: 25 * level,
						alignSelf: "center",
					}}
					width={20}
					height={20}
				></DownArrow>
			);
		} else {
			return (
				<RightArrow
					style={{
						marginLeft: 25 * level,
						alignSelf: "center",
					}}
					width={20}
					height={20}
				></RightArrow>
			);
		}
	};

	isEndend = (hasChildrenNodes, margin) => {
		if (!hasChildrenNodes) {
			return (
				<Text style={{ color: "#1590FF", marginLeft: margin }}>
					{">"} Placer ici {"<"}
				</Text>
			);
		}
	};
	render() {
		return (
	
				
			<SafeAreaView>
				<TreeView
					style={styles.treeView}
					data={this.props.dataTree}
					childrenKey="sub_folders"
					idKey="title"
					renderNode={({ node, level, isExpanded, hasChildrenNodes }) => {
						return (
							<SafeAreaView style={styles.nodeTree}>
								<SafeAreaView style={{ flexDirection: "row" }}>
									{this.getIndicator(isExpanded, hasChildrenNodes, level)}
									<LittleFolder
										style={{
											marginLeft: 12,
										}}
										width={30}
										height={30}
										fill={node.color ? node.color : "#1590FF"}
									></LittleFolder>
									<Text
										style={{
											marginLeft: 12,
											fontSize: 20,
										}}
									>
										{node.title}
									</Text>
								</SafeAreaView>
								<SafeAreaView style={{}}>
									{this.isEndend(hasChildrenNodes, 12)}
								</SafeAreaView>
							</SafeAreaView>
						);
					}}
				/>
			</SafeAreaView>
					

		);
	}
}

const styles = StyleSheet.create({

	treeView: {
		backgroundColor: "red",
	},

	nodeTree: {
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		marginVertical: 10,
	},

});

export default PathComp;
