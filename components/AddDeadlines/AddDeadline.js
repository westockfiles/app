import React from "react";

import {
	SafeAreaView,
	StyleSheet,
	Text,
	FlatList,
	Alert,
	Modal,
} from "react-native";
import Popup from "../Misc/Popups/Popup";
import DeadlineData from "../Deadline/DeadlinesData";

class AddDeadline extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			display_popup: 0,
			deadlines_data: DeadlineData["data"],
			description_deadline: "",
		};

		this.popups = [
			{
				closeHandler: () => {
					this.setState({
						//Add The save of the daedline
						display_popup: -1,
					});
				},
				title: "Ajouter une deadline",
				fields: [
					{
						name: "input",
						input_name: "description_deadline",
						input_icon: "",
						input_value: "",
						input_opt_icon: false,
						input_rounded: false,
						input_label: "Description ...",
						input_secure: false,
						input_can_show: false,
						inputHandler: (name, event) => {
							this.popups[0].fields[0].input_value = event.nativeEvent.text;
							this.setState({
								description_deadline: this.popups[0].fields[0].input_value,
							});
							console.log(this.state.description_deadline);
						},
						focusHandler: (event) => {},
					},
					{
						name: "calendar",

						saveDate: (day) => {
							let time_remaining = day.timestamp - Date.now();
							time_remaining = time_remaining / 1000;
							this.setState({
								selected_date: {
									id: this.state.deadlines_data.length,
									time: time_remaining.toString(),
									description: this.state.description_deadline,
								},
							});
						},
					},
				],

				button_title: "Créer une deadline",
				pressHandler: (evt) => {
					this.state.selected_date.description =
						this.state.description_deadline;
					this.state.deadlines_data.push(this.state.selected_date);
					console.log(this.state.deadlines_data);
					this.setState({ display_popup: -1 });
				},
			},
		];
	}

	componentDidMount() {
		var today = new Date();
		var dd = String(today.getDate()).padStart(2, "0");
		var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
		var yyyy = today.getFullYear();
		today = yyyy + "-" + mm + "-" + dd;

		this.setState({
			marked_date: {
				[today]: { selected: true, marked: true, selectedColor: "blue" },
			},
		});
	}

	render() {
		return (
			<SafeAreaView style={styles.container}>
				<Modal
					animationType="fade"
					transparent={true}
					visible={this.state.display_popup != -1}
				>
					<Popup popup={this.popups[this.state.display_popup]}></Popup>
				</Modal>
			</SafeAreaView>
		);
	}
}

const styles = StyleSheet.create({});

export default AddDeadline;
