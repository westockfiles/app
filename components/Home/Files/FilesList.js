import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import FileItem from './FileItem';
import profiles from '../../../src/imgs/profiles';

/**
 * Class FileList
 * props : 
 *  - files : list of files
 *  - width : width of an item
 */

class FilesList extends React.Component{
    render(){
        return(
            <SafeAreaView style={styles.container}>
                {
                    this.props.files.map((file, id)=>{
                        return(
                            <FileItem key={id} width={this.props.width} img={profiles[file.img]} name={file.name} date={file.date}></FileItem>
                        )
                    })
                }
            </SafeAreaView>
        )
    }
}

const styles= StyleSheet.create({
    container :{
        flex : 1,
        flexDirection : "row",
        flexWrap : "wrap",
    }
})

export default FilesList;