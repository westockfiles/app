import React from 'react';
import { SafeAreaView, StyleSheet, Text, Image } from 'react-native';

/**
 * Class FileItem
 * props : 
 *  - img : source of image
 *  - width : width of the container
 *  - name : file's name
 *  - date : file's date
 */
class FileItem extends React.Component{
    
    render(){
        return(
            <SafeAreaView style={[styles.container, { width : this.props.width}]}>
                <Image style={styles.img} source={this.props.img} />
                <SafeAreaView style={styles.banner}>
                    <Text style={styles.name}>{this.props.name}</Text>
                    <Text style={styles.date}>{this.props.date}</Text>
                </SafeAreaView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        borderRadius : 30,
        marginLeft : 20,
        marginBottom : 20,
        height : 200,
        overflow : "hidden",
        shadowColor: "#000",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 5,
    },
    img:{
        width : "100%",
        height : "100%",
    },
    banner:{
        position : "absolute",
        bottom : 0,
        left : 0,
        zIndex : 1,
        width : "100%",
        height : "33%",
        backgroundColor : "#fff",
        paddingLeft : "10%",
        justifyContent : "center"
    },
    name:{
        fontSize : 14,
        color : "#000",
        fontWeight : "bold"
    },
    date:{
        fontSize : 12
    }
})

export default FileItem;