import React from 'react'
import { SafeAreaView, StyleSheet } from 'react-native'
import ShortcutItem from './ShortcutItem'

/**
 * Class ShortcutsList
 * props :
 *  - pressAddFolder, function triggered on press event
 *  - pressCamere, function trigerred on press event
 */
class ShortcutsList extends React.Component{
    constructor(props){
        super(props);
        this.shortcuts=[
            {
                icon : 'addFolder',
                onPressOut : {
                    color : "#1599ff",
                    backgroundColor : "#fff"
                },
                onPressIn:{
                    color : "#fff",
                    backgroundColor : "#2d9bff"
                },
                onPress: this.props.pressAddFolder
            },
            {
                icon : 'camera',
                onPressOut : {
                    color : "#fff",
                    backgroundColor : "#1599ff"
                },
                onPressIn:{
                    color : "#fff",
                    backgroundColor : "#2d9bff"
                },
                onPress: this.props.pressCamera
            }
        ];
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                {
                    this.shortcuts.map((shortcut, id)=>{
                        return(
                            <ShortcutItem key={id} shortcut={shortcut}></ShortcutItem>
                        )
                    })
                }
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        position : "absolute",
        bottom : 0,
        paddingRight : 25,
        alignSelf: "flex-end",
        flexWrap : "wrap"
    }
})

export default ShortcutsList;