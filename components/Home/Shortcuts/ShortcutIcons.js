const ShortcutIcons ={
    addFolder : require('../../../src/icons/addFolder.svg').default,
    camera : require('../../../src/icons/camera.svg').default
}

export default ShortcutIcons;