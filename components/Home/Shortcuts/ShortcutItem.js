import React from 'react'
import { SafeAreaView } from 'react-native'
import CustomIconButton from '../../Misc/CustomIconButton'
import ShortcutIcons from "./ShortcutIcons";

/**
 * Class ShortccutItem
 * props :
 *  - shortcut, shortcut element 
 */
class ShortcutItem extends React.Component{
    constructor(props){
        super(props);
        this.Icon = ShortcutIcons[this.props.shortcut.icon];
    }

    render(){
        return(
            <SafeAreaView style={{marginBottom : 15}}>
                <CustomIconButton press={this.props.shortcut.onPress} Icon={this.Icon} width={25} height={25} shadow={true} pressOut={this.props.shortcut.onPressOut} pressIn={this.props.shortcut.onPressIn}></CustomIconButton>
            </SafeAreaView>
        )
    }
}

export default ShortcutItem;