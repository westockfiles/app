import React from 'react'
import { SafeAreaView, StyleSheet, Text } from 'react-native'
import CustomTextButton from '../Misc/CustomTextButton'
import DeadlineAlertIcon from '../../src/icons/deadlinesAlert.svg'
import DeadlinesData from '../Deadline/DeadlinesData';

/**
 * Class DeadlineAlert
 * props :
 *  - pressHandler, handler triggered on press event
 */
class DeadlineAlert extends React.Component{

    constructor(props){
        super(props);
        this.styles = StyleSheet.create({
            container:{
                width : "100%",
                flexDirection : "row",
                backgroundColor : DeadlinesData.data[0].time < 172800 ? "#ff5d55" : DeadlinesData.data[0].time < 604800 ? "#ffac48" : "#1ea832",
                padding : 20,
                borderRadius : 25,
                justifyContent : "space-around",
                marginBottom : 50,
                alignSelf : "center",
            },
            sub_container:{
                flexDirection:"column",
                justifyContent : "space-around"
            },
            title:{
                color : "#fff",
                fontSize : 16,
            }
        })
        if(DeadlinesData.data[0].time < 172800){
            this.title = "Deadlines imminentes !";
        }else if(DeadlinesData.data[0].time < 604800){
            this.title = "Deadlines en approche !";
        }else{
            this.title = "Vous avez le temps !";
        }
    }

    render(){
        return(
            <SafeAreaView style={this.styles.container}>
                <SafeAreaView style={this.styles.sub_container}>
                    <Text style={this.styles.title}>{this.title}</Text>
                    <CustomTextButton title="Afficher mes deadlines" color="white-0.2" padTop={7} padBot={7} handler={this.props.pressHandler} invert></CustomTextButton>
                </SafeAreaView>
                <DeadlineAlertIcon width={80} height={80}></DeadlineAlertIcon>         
            </SafeAreaView>
        )
    }
}

export default DeadlineAlert