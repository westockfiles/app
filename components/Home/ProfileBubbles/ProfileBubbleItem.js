import React from 'react'
import { SafeAreaView, StyleSheet, Image } from 'react-native';
import profiles from '../../../src/imgs/profiles';
import CustomIconButton from '../../Misc/CustomIconButton';
import ProfileBubble from './ProfileBubble';
import Cross from '../../../src/icons/cross.svg';

/**
 * Class ProfileBubbleItem
 * props :
 *  - index, index of element in array,
 *  - deleteHandler, function triggered on delete
 *  - nbProfiles, number of profile
 *  - marginLeft, offset left,
 *  - marginRight, offset right,
 *  - canBeDeleted, boolean which indeicates if element can be deleted
 *  - src, source of image
 */
class ProfileBubbleItem extends React.Component{
    constructor(props){
        super(props);
        this.index = this.props.index;
    
        /**
         * getIndex, get index of element on delete
         * @param {*} evt 
         */
        this.getIndex=(evt)=>{
            this.props.deleteHandler(evt, this.index);
        }
    }

    render(){
        return(
            <SafeAreaView style={{zIndex: this.props.nbProfiles - this.props.index, marginLeft : this.props.marginLeft && this.props.index != 0 ? this.props.marginLeft : 0, marginRight : this.props.marginRight ? this.props.marginRight : 0}}>
                {
                    this.props.canBeDeleted &&
                    <SafeAreaView style={styles.close}>
                        <CustomIconButton press={this.getIndex} Icon={Cross} width={7} height={7} padding={7} shadow={true} pressOut={{color : "#fff", backgroundColor : "#ff5d55"}} pressIn={{color : "#fff", backgroundColor : "#ff6f68"}}></CustomIconButton>
                    </SafeAreaView>
                }
                <ProfileBubble width={this.props.width} height={this.props.height} src={profiles[this.props.src]}></ProfileBubble>
            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    close:{
        position : "absolute",
        alignSelf : 'flex-end',
        top : -9,
        right : -9,
        zIndex : 100
    }
})

export default ProfileBubbleItem;