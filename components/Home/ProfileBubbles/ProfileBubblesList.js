import React from 'react'
import { SafeAreaView, StyleSheet } from 'react-native'
import ProfileBubbleItem from './ProfileBubbleItem';

/**
 * Class PorfileBubblesList
 * props :
 *  - profiles, list of profiles,
 *  - width, width of an element,
 *  - height, height of an element,
 *  - marginleft, left offset,
 *  - marginRight, right offset,
 *  - nbProfiles, number of profile
 *  - canBeDeleted, boolean which indecates ifelement can be deleted,
 *  - deleteHandler, function triggered on delete
 */
class ProfileBubblesList extends React.Component{

    render(){
        return(
            <SafeAreaView style={styles.container}>
                {
                    this.props.profiles.map((profile, id)=>{
                        return(
                            <ProfileBubbleItem key={id} width={this.props.width} height={this.props.height} index={id} src={profile} marginLeft={this.props.marginLeft} marginRight={this.props.marginRight} nbProfiles={this.props.profiles.length} canBeDeleted={this.props.canDelete} deleteHandler={this.props.deleteHandler}></ProfileBubbleItem>
                        )
                    })
                }
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flexDirection : "row",
        alignSelf : "flex-start",
        flexWrap : "wrap"
    }
})

export default ProfileBubblesList;