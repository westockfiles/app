import React from 'react'
import { SafeAreaView, StyleSheet, Image } from 'react-native';

/**
 * Class ProfileBubble
 * props :
 *  - width : width of bubble
 *  - height : height of bubble
 *  - src : source of the image
 */
class ProfileBubble extends React.Component{
    constructor(props){
        super(props);
        this.styles = StyleSheet.create({
            img:{
                position : 'relative',
                borderRadius : 25,
                width : this.props.width,
                height : this.props.height
            }
        });
    }

    render(){
        return(
            <SafeAreaView>
                <Image style={this.styles.img} source={this.props.src}></Image>
            </SafeAreaView>
        )
    }
}

export default ProfileBubble;