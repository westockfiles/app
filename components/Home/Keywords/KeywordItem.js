import React from 'react'
import { SafeAreaView, StyleSheet, Text } from 'react-native'
import CustomIconButton from '../../Misc/CustomIconButton'
import Cross from '../../../src/icons/cross.svg';

/**
 * Class KeywordItem
 * props :
 *  - index, index of keyword in the list,
 *  - keyword, keyword element,
 *  - canBeDeleted, boolean which indicates if element can be deleted,
 *  - deleteHandler, function triggered on delete,
 */
class KeywordItem extends React.Component{
    constructor(props){
        super(props);
        this.index = this.props.index;
        
        /**
         * getIndex(), get the index of the element on delete
         * @param {*} evt 
         */
        this.getIndex=(evt)=>{
            this.props.deleteHandler(evt, this.index);
        }
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                {
                    this.props.canBeDeleted &&
                    <SafeAreaView style={styles.close}>
                        <CustomIconButton press={this.getIndex} Icon={Cross} width={7} height={7} padding={7} shadow={true} pressOut={{color : "#fff", backgroundColor : "#ff5d55"}} pressIn={{color : "#fff", backgroundColor : "#ff6f68"}}></CustomIconButton>
                    </SafeAreaView>
                }
                <Text>#{this.props.keyword}</Text>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        borderWidth : 1,
        borderRadius : 25,
        alignSelf : 'flex-start',
        paddingHorizontal : 10,
        paddingTop : 3,
        paddingBottom : 5,
        marginRight : 10,
        marginBottom : 10,
        borderColor : "#707070"
    },
    close:{
        position : "absolute",
        alignSelf : 'flex-end',
        top : -9,
        right : -9
    },
})

export default KeywordItem;