import React from 'react'
import { SafeAreaView, StyleSheet } from 'react-native'
import CustomIconButton from '../../Misc/CustomIconButton'
import KeywordItem from './KeywordItem'
import Plus from '../../../src/icons/plus.svg'

/**
 * Class KeywordsList
 * props :
 *  - keywords, list of keyword,
 *  - canDelete, boolean which indeicates if element can be deleted
 *  - press, function triggered on press event
 */
class KeywordsList extends React.Component{
    render(){
        return(
            <SafeAreaView style={styles.container}>
                {
                    this.props.keywords.map((keyword,id)=>{
                        return(
                            <KeywordItem key={id} index={id} keyword={keyword} canBeDeleted={this.props.canDelete} deleteHandler={this.props.deleteHandler}></KeywordItem>
                        )
                    })
                }
                {
                    !this.props.canDelete &&
                    <CustomIconButton press={this.props.press} Icon={Plus} width={15} height={15} padding={7} shadow={false} pressOut={{color : "#707070", backgroundColor : "#fff"}} pressIn={{color : "#cacaca", backgroundColor : "#fff"}}></CustomIconButton>
                }
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex : 1,
        flexDirection : "row",
        alignSelf : "flex-start",
        flexWrap : "wrap",
    }
})

export default KeywordsList;