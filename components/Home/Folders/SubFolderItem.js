import React from 'react';
import { Pressable, SafeAreaView, StyleSheet, Text } from 'react-native';
import LittleFolder from '../../../src/icons/littleFolder.svg'

/**
 * Class SubFolderItem
 * props :
 *  - pressHandler, function triggered on press event
 *  - index, index of the subfolder in list
 *  - longpressHandler, function triggered on long press event
 *  - bottomBorder, boolean which indicates if are bottom border
 *  - subfolder, subfolder element
 */
class SubFolderItem extends React.Component{
    render(){
        return(
            <Pressable
                onPress={(evt)=>this.props.press(evt, this.props.index)}
                onLongPress={evt=>this.props.longPress(evt, this.props.index)}
            >
                <SafeAreaView style={[styles.container, {borderBottomWidth : this.props.bottomBorder ? 1 : 0}]}>
                    <LittleFolder width={40} height={40} fill={this.props.subFolder.color}></LittleFolder>
                    <SafeAreaView style={styles.texts}>
                        <Text style={styles.sub_folder_name}>{this.props.subFolder.title}</Text>
                        <Text style={styles.sub_folder_info}>{this.props.subFolder.info}</Text>
                    </SafeAreaView>
                    <Text style={styles.nb_file}>{this.props.subFolder.nb_file}</Text>
                </SafeAreaView>
            </Pressable>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flexDirection : "row",
        alignItems : "center",
        justifyContent : "center",
        paddingBottom : 20,
        borderColor : "#cacaca",
        paddingHorizontal : 15
    },
    texts:{
        flex : 1,
        justifyContent : "center",
        paddingLeft : 30
    },
    sub_folder_name:{
        fontSize : 18,
        color : "#000",
        fontWeight : "bold"
    },
    sub_folder_info:{
        fontSize : 12,
    },
    nb_files:{
        fontSize : 18,
        alignSelf : "center",
    }
})

export default SubFolderItem;