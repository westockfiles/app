import React from 'react'
import { SafeAreaView, StyleSheet, Text } from 'react-native'
import AddFolder from './AddFolder'
import FolderItem from './FolderItem'

/**
 * Class FolderList
 * props :
 *  - folders, list of folder
 */
class FoldersList extends React.Component{
    render(){
        return(
            <SafeAreaView>
                <Text style={styles.title}>Tous les dossiers</Text>
                <SafeAreaView style={styles.list}>
                    {
                        this.props.folders.map((folder, id)=>{
                            return(
                                <FolderItem key={id} index={id} folder={folder} pressHandler={this.props.pressHandler} longPressHandler={this.props.longPressHandler}></FolderItem>
                            )
                        })
                    }
                    <AddFolder press={this.props.addFolder}></AddFolder>
                </SafeAreaView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    title:{
        fontWeight : "bold",
        color : "#000",
        fontSize : 20,
        marginBottom : 30
    },
    list:{
        flexDirection : "row",
        justifyContent : 'space-between',
        flexWrap : "wrap"
    }
})

export default FoldersList