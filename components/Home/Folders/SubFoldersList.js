import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import SubFolderItem from './SubFolderItem';

/**
 * Class SubFoldersList
 * props :
 *  - subFodlers, list of subfolder
 *  - press, function triggered on press event
 *  - longPress, function triggered on long press event
 */
class SubFoldersList extends React.Component{    
    render(){
        return(
            <SafeAreaView style={styles.container}>
                {
                    this.props.subFolders.map((sub_folder, id)=>{
                        return(
                            <SubFolderItem key={id} index={id} bottomBorder={id+1==this.props.subFolders.length} subFolder={sub_folder} press={this.props.pressHandler} longPress={this.props.longPressHandler}></SubFolderItem>
                        )
                    })
                }
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        marginHorizontal : 5
    }
})

export default SubFoldersList;