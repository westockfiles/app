import React from 'react'
import { SafeAreaView, StyleSheet, Text } from 'react-native';
import Pressable from 'react-native/Libraries/Components/Pressable/Pressable';
import Folder from '../../../src/imgs/folder.svg'
import ProfileBubblesList from '../ProfileBubbles/ProfileBubblesList';

/**
 * Class FolderItem
 * props : 
 *  - index, indes of folder in the list
 *  - folder, folder to display
 *  - press, function trigger on press
 *  - longPress, function trigger on long press
 */
class FolderItem extends React.Component{
    constructor(props){
        super(props);
        this.state={
            opacity : 1
        };
        /**
         * pressIn(), triggered on press in
         */
        this.pressIn=()=>{
            this.setState({opacity : 0.9});
        };

        /**
         * pressOut(), triggered on press out
         */
        this.pressOut=()=>{
            this.setState({opacity : 1});
        };

        this.styles = StyleSheet.create({
            body:{
                position : 'relative',
                zIndex : 2,
                marginTop : -110,
                marginLeft : 18,
                marginBottom : 30
            },
            inside:{
                marginTop : this.props.folder.profiles.length > 0 ? 0 : 30,
            },
            title:{
                fontSize : 25,
                fontWeight : "bold",
                marginBottom : 5,
                color : "#fff"
            },
            sub_title:{
                color : "#fff",
                marginBottom : 10
            }
        });
    }

    render(){
        return(
            <SafeAreaView style={{flex : 0}}>
                <Pressable
                    style={{opacity : this.state.opacity}}
                    onPress={(evt)=> this.props.pressHandler(evt, this.props.index)}
                    onPressIn={this.pressIn}
                    onPressOut={this.pressOut}
                    onLongPress={(evt)=>this.props.longPressHandler(evt, this.props.index)}
                >
                    <Folder width="150" fill={this.props.folder.color}></Folder>
                    <SafeAreaView style={this.styles.body}>
                        <SafeAreaView style={this.styles.inside}>
                            <Text style={this.styles.title}>{this.props.folder.title}</Text>
                            <Text style={this.styles.sub_title}>{this.props.folder.nb_file} fichiers</Text>
                            {
                                this.props.folder.profiles.length > 0 &&
                                <ProfileBubblesList width={20} height={20} profiles={this.props.folder.profiles} marginLeft={-5}></ProfileBubblesList>
                            }
                        </SafeAreaView>
                    </SafeAreaView>
                </Pressable>
            </SafeAreaView>
        )
    }
}

export default FolderItem;