import React from 'react'
import { SafeAreaView } from 'react-native';
import Pressable from 'react-native/Libraries/Components/Pressable/Pressable';
import Folder from '../../../src/imgs/addFolder.svg'

/**
 * Class AddFolder
 * props :
 *  - press : function triggered on press event
 */
class AddFolder extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            color : "#dedede"
        };

        /**
         * pressIn(), function trigger on press in event
         */
        this.pressIn=()=>{
            this.setState({color : "#2d9bff"});
        };

        /**
         * pressOut(), function trigger on press out event
         */
        this.pressOut=()=>{
            this.setState({color : "#dedede"});
        };
    }

    render(){
        return(
            <SafeAreaView>
                <Pressable
                    onPress={this.props.press}
                    onPressIn={this.pressIn}
                    onPressOut={this.pressOut}
                >
                    <Folder width="150" fill={this.state.color}></Folder>
                </Pressable>
            </SafeAreaView>
        )
    }
}

export default AddFolder;