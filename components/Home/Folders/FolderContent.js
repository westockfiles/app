import React from 'react'
import { Keyboard, SafeAreaView, ScrollView, StyleSheet, Text, Modal } from 'react-native'
import SubFoldersList from './SubFoldersList';
import FilesList from '../Files/FilesList';
import ShortcutsList from '../Shortcuts/ShortcutsList';
import Popup from '../../Misc/Popups/Popup';

/**
 * Class FolderContent
 * props :
 *  - route : navigation route
 */
class FolderContent extends React.Component{
    constructor(props){
        super(props);

        this.state={
            display_popup : -1,
            
            sub_folder:{
                color : "",
                title : "",
                info : "",
                nb_file : 0,
                sub_folders : [],
                keywords : [],
                files : [],
                path : []
            }
        };

        this.sub_folders = [];

        this.files = [];

        this.parent = null;

        this.popups=[
            {
                closeHandler : ()=>{
                    this.setState({sub_folder : {color : "", title : "", info : "", nb_file : 0, sub_folders : [], keywords : [], items : []}, display_popup : -1});
                },
                title : "Paramètres du dossier",
                fields : [
                    {
                        name : "input",
                        input_name : "folder_title",
                        input_value : "",
                        input_icon : "",
                        input_opt_icon : false,
                        input_rounded : false,
                        input_label : "Titre du dossier",
                        input_secure : false,
                        input_can_show : false,
                        inputHandler : (name, event)=>{
                            const sub_folder = this.state.sub_folder;
                            sub_folder.title = event.nativeEvent.text;
                            this.popups[0].fields[0].input_value = sub_folder.title;
                            this.setState({sub_folder : sub_folder});
                        },
                        focusHandler : (event)=>{
    
                        }
                    },
                    {
                        name : "colors",
                        colors_title : "Couleur du dossier :",
                        colors_colors : [
                            "#1599ff",
                            "#ff5d55",
                            "#ffac48",
                            "#1ea832"
                        ],
                        handlerAdd:()=>{
                            this.setState({display_popup : 1});
                        },
                        getColorSel:(color)=>{
                            const sub_folder = this.state.sub_folder;
                            sub_folder.color = color;
                            this.setState({sub_folder : sub_folder});
                        }
                    },
                    {
                        name : "keywords",
                        keywords_title : "Mots clés associés :",
                        keywords_keywords : [
                        ],
                        keywords_can_delete : false,
                        handlerAdd : ()=>{
                            this.popups[2].fields[1].keywords_keywords = [...this.popups[0].fields[2].keywords_keywords];
                            this.setState({display_popup : 2});
                        }
                    }
                ],
                button_title : "Créer un dossier",
            },
            {
                closeHandler : ()=>{
                    this.setState({display_popup : 0});
                },
                title : "Créer votre couleur :",
                fields : [
                    {
                        name : "colorPicker",
                        getColor:(rgb)=>{
                            this.setState({newColor : rgb})
                        }
                    }
                ],
                button_title : "Ajouter la couleur",
                pressHandler:(evt)=>{
                    const sub_folder = this.state.sub_folder;
                    sub_folder.color = this.state.newColor;
                    this.popups[0].fields[1].colors_colors.push(this.state.newColor);
                    this.setState({sub_folder : sub_folder, display_popup : 0});
                }
            },
            {
                closeHandler : ()=>{
                    this.popups[2].fields[0].input_value = "";
                    this.popups[2].fields[1].keywords_keywords = [];
                    Keyboard.dismiss();
                    this.setState({display_popup : 0});
                },
                title : "Mots clés associés :",
                fields : [
                    {
                        name : "input",
                        input_name : "keyword",
                        input_value : "",
                        input_icon : "",
                        input_opt_icon : {
                            name : "plus",
                            color : "#1599ff",
                            width : 20,
                            height : 20,
                            pressHandler:(event)=>{
                                const new_keywords = this.popups[2].fields[0].input_value.split(",");
                                new_keywords.forEach(new_keyword=>{
                                    if(this.popups[2].fields[1].keywords_keywords.find(keyword=>{return keyword == new_keyword.toLowerCase().trim()}) === undefined){
                                        this.popups[2].fields[1].keywords_keywords.push(new_keyword.toLowerCase().trim());
                                    }
                                });
                                this.popups[2].fields[0].input_value = "";
                                this.setState({display_popup : 2});
                            }
                        },
                        input_rounded : false,
                        input_label : "Ajouter un mot clé",
                        input_secure : false,
                        input_can_show : false,
                        inputHandler : (name, event)=>{
                            this.popups[2].fields[0].input_value = event.nativeEvent.text;
                            this.setState({});
                        },
                        focusHandler : (event)=>{
    
                        }
                    },
                    {
                        name : "keywords",
                        keywords_title : "Mots clés associés :",
                        keywords_keywords : [
                        ],
                        keywords_can_delete : true,
                        deleteHandler:(evt, index)=>{
                            this.popups[2].fields[1].keywords_keywords.splice(index, 1);
                            this.setState({display_popup : 2});
                        }
                    }
                ],
                button_title : "Valider",
                pressHandler:(evt)=>{
                    Keyboard.dismiss();
                    this.popups[2].fields[0].input_value = "";
                    this.popups[0].fields[2].keywords_keywords = [...this.popups[2].fields[1].keywords_keywords];
                    this.setState({display_popup : 0});
                }
            }
        ];

        /**
         * folderPressHandler(), function triggered on press event
         * @param {*} event 
         * @param {*} index 
         */
        this.folderPressHandler=(event, index)=>{
            this.props.navigation.navigate('FolderContent',{
                breadcrumb : [...this.props.route.params.breadcrumb, index],
                folder : this.sub_folders[index],
            });
        };

        /**
         * pressAddFolder(), function triggered on press event
         */
        this.pressAddFolder=()=>{
            this.popups[0].fields[0].input_value = "";
            this.popups[0].fields[2].keywords_keywords = [];
            this.popups[0].button_title = "Créer le dossier";
            this.popups[0].suppr = 0;
            this.popups[0].pressHandler=(evt)=>{
                const sub_folder = {...this.state.sub_folder};
                const date = new Date()
                const months = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"]
                sub_folder.info = `Modifié le ${date.getDate()} ${months[parseInt(date.getMonth())]} ${date.getFullYear()}`;
                sub_folder.path=[...this.props.route.params.folder.path, this.props.route.params.folder.title];
                this.sub_folders.push(sub_folder);
                this.setState({sub_folder : {color : "", title : "", info : "", nb_file : 0, sub_folders : [], keywords:[], items : []}, display_popup : -1});
            }
            this.setState({sub_folder : {color : "", title : "", nb_file : 0, keywords : [],  sub_folders:[], keywords:[], files:[]}, display_popup : 0});
        };

        /**
         * pressCamera, triggered on press event
         */
        this.pressCamera=()=>{

        };

        /**
         * longPressHandler(), function triggered on long press event
         * @param {*} event 
         * @param {*} index 
         */
        this.longPressHandler=(event, index)=>{
            const sel_folder = {};
            Object.assign(sel_folder, this.sub_folders[index]);
            this.popups[0].fields[0].input_value = sel_folder.title;
            if(this.popups[0].fields[1].colors_colors.find(color=>color == sel_folder.color) == undefined){
                this.popups[0].fields[1].colors_colors.push(sel_folder.color);
            }
            this.popups[0].fields[2].keywords_keywords = [...sel_folder.keywords];
            this.popups[0].button_title = "Modifier le dossier";
            this.popups[0].suppr = 1;
            this.popups[0].supprHandler=(evt)=>{
                this.sub_folders.splice(index, 1);
                this.setState({sub_folder : {color : "", title : "", nb_file : 0, keywords : [], sub_folders:[], files:[]}, display_popup : this.state.display_popup -1});
            };
            this.popups[0].pressHandler=(evt)=>{
                Object.assign(this.sub_folders[index],sel_folder);
                this.setState({sub_folder : {color : "", title : "", nb_file : 0, keywords : [], sub_folders:[], files:[]}, display_popup : this.state.display_popup -1});
            }
            this.setState({sub_folder : sel_folder, display_popup : 0});
        };
    }

    /**
     * componentDidMount(),call when components is mount
     */
    componentDidMount(){
        this.sub_folders = this.props.route.params.folder.sub_folders;
        this.files = this.props.route.params.folder.files;
        this.setState({});
    }

    /**
     * componentDidUpdate(),call when components is update
     * @param {*} prevProps
     */
    componentDidUpdate(prevProps){
        if(this.props.route.params.folder.title != prevProps.route.params.folder.title){
            this.componentDidMount();
        }
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.display_popup !=-1}
                >
                    <Popup popup={this.popups[this.state.display_popup]} folder={this.state.sub_folder}></Popup>
                </Modal>
                <ScrollView style={styles.body}>
                    <Text style={styles.title}>{this.props.route.params.folder.title}</Text>
                    <Text style={styles.sub_title}>Dossiers</Text>
                    <SubFoldersList subFolders={this.sub_folders} pressHandler={this.folderPressHandler.bind(this)} longPressHandler={this.longPressHandler.bind(this)}></SubFoldersList>
                    <Text style={styles.sub_title}>Fichiers</Text>
                    <FilesList files={this.files} width={"40%"}></FilesList>
                </ScrollView>
                <ShortcutsList pressAddFolder={this.pressAddFolder} pressCamera={this.pressCamera}></ShortcutsList>
            </SafeAreaView>
        )
    }
}

//<Banner Icon={SearchIcon} profile={require('../../../src/imgs/profile1.jpg')} pressIn={{color : "#707070", backgroundColor : "#fff"}} pressOut={{color : "#707070", backgroundColor : "#fff"}}></Banner>

const styles = StyleSheet.create({
    container:{
        flex : 1,
    },
    body:{
        flex : 1,
        paddingHorizontal : 25,
    },
    title:{
        fontSize : 30,
        fontWeight : "bold",
        color : "#000",
        marginTop : 30
    },
    sub_title:{
        fontSize : 24,
        marginVertical : 30
    }
})

export default FolderContent;