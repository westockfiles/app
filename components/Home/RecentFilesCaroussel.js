import React from 'react'
import { SafeAreaView, StyleSheet, Text } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';
import FilesList from './Files/FilesList';

/**
 * Class DeadlineAlert
 * props :
 *  - 
 */
class RecentFileCaroussel extends React.Component{
    constructor(props){
        super(props);
        this.files= [
            {
                img : "profile1",
                name: "Photo de belette 1",
                date: "21/02/2021",
                extension : ".jpg"
            },{
                img : "profile2",
                name: "Photo de belette 2",
                date: "21/02/2021",
                extension : ".jpg"
            },
            {
                img : "profile3",
                name: "Photo de belette 3",
                date: "21/02/2021",
                extension : ".jpg"
            }
        ];
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <Text style={styles.title}>Ouvert récemment</Text>
                <ScrollView
                    style = {styles.sub_container}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                >
                    <FilesList files={this.files} width={150}></FilesList>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex : 1,
        marginBottom : 25
    },
    title:{
        fontWeight : "bold",
        color : "#000",
        fontSize : 20,
        marginBottom : 30
    },
    test:{
        borderRadius : 30,
        marginLeft : 20,
        marginBottom : 20,
        height : 200,
        width : 150,
        overflow : "hidden",
        shadowColor: "#000",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 5,
    },
})

export default RecentFileCaroussel