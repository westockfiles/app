import React from 'react';
import { Keyboard, Modal, SafeAreaView, ScrollView, StyleSheet } from 'react-native';
import FoldersList from './Folders/FoldersList';
import ShortcutsList from './Shortcuts/ShortcutsList';
import Popup from '../Misc/Popups/Popup';
import folderTree from '../Misc/folderTree';
import DeadlineAlert from './DeadlineAlert';
import RecentFileCaroussel from './RecentFilesCaroussel';

/**
 * Class Home
 * props :
 */
class Home extends React.Component{
    constructor(props){
        super(props);
        this.state={
            display_popup : -1,
            
            folder:{
                color : "",
                title : "",
                nb_file : 0,
                profiles : [], 
                sub_folders:[],
                files:[]
            }
        };
    
        this.folders= folderTree.sub_folders;
    
        this.popups=[
            {
                closeHandler : ()=>{
                    this.setState({folder : {color : "", title : "", nb_file : 0, profiles : [], sub_folders:[], files:[]}, display_popup : -1});
                },
                title : "Paramètres du dossier",
                fields : [
                    {
                        name : "input",
                        input_name : "folder_title",
                        input_icon : "",
                        input_value : "",
                        input_opt_icon : false,
                        input_rounded : false,
                        input_label : "Titre du dossier",
                        input_secure : false,
                        input_can_show : false,
                        inputHandler : (name, event)=>{
                            const folder = this.state.folder;
                            folder.title = event.nativeEvent.text;
                            this.popups[0].fields[0].input_value = folder.title;
                            this.setState({folder : folder});
                        },
                        focusHandler : (event)=>{
    
                        }
                    },
                    {
                        name : "colors",
                        colors_title : "Couleur du dossier :",
                        colors_colors : [
                            "#1599ff",
                            "#ff5d55",
                            "#ffac48",
                            "#1ea832"
                        ],
                        handlerAdd:()=>{
                            this.setState({display_popup : 1});
                        },
                        getColorSel:(color)=>{
                            const folder = this.state.folder;
                            folder.color = color;
                            this.setState({folder : folder});
                        }
                    },
                    {
                        name : "profiles",
                        profiles_title : "Partage du dossier :",
                        profiles_profiles : [
                        ],
                        handlerAdd : ()=>{
                            this.popups[2].fields[1].profiles_profiles = [...this.popups[0].fields[2].profiles_profiles];
                            this.setState({display_popup : 2});
                        }
                    },
                    {
                        name : "keywords",
                        keywords_title : "Mots clés associés :",
                        keywords_keywords : [
                        ],
                        keywords_can_delete : false,
                        handlerAdd : ()=>{
                            this.popups[3].fields[1].keywords_keywords = [...this.popups[0].fields[3].keywords_keywords];
                            this.setState({display_popup : 3});
                        }
                    }
                ],
                button_title : "Créer un dossier",
            },
            {
                closeHandler : ()=>{
                    this.setState({display_popup : 0});
                },
                title : "Créer votre couleur",
                fields : [
                    {
                        name : "colorPicker",
                        getColor:(rgb)=>{
                            this.setState({newColor : rgb})
                        }
                    }
                ],
                button_title : "Ajouter la couleur",
                pressHandler:(evt)=>{
                    const folder = this.state.folder;
                    folder.color = this.state.newColor;
                    this.popups[0].fields[1].colors_colors.push(this.state.newColor);
                    this.setState( {folder : folder, display_popup : 0});
                }
            },
            {
                closeHandler : ()=>{
                    this.popups[2].fields[0].input_value = "";
                    this.popups[2].fields[1].profiles_profiles = [];
                    Keyboard.dismiss();
                    this.setState({display_popup : 0});
                },
                title : "Ajouter un compte",
                fields : [
                    {
                        name : "input",
                        input_name : "mail_username",
                        input_icon : "",
                        input_value : "",
                        input_opt_icon : {
                            name : "plus",
                            color : "#1599ff",
                            width : 20,
                            height : 20,
                            pressHandler:(event)=>{
                                const new_profiles = this.popups[2].fields[0].input_value.split(",");
                                new_profiles.forEach(new_profile=>{
                                    if(this.popups[2].fields[1].profiles_profiles.find(profile=>{return profile == new_profile.toLowerCase().trim()}) === undefined){
                                        this.popups[2].fields[1].profiles_profiles.push(new_profile.toLowerCase().trim());
                                    }
                                });
                                this.popups[2].fields[0].input_value = "";
                                this.setState({});
                            }
                        },
                        input_rounded : false,
                        input_label : "Identifiant ou Email",
                        input_secure : false,
                        input_can_show : false,
                        inputHandler : (name, event)=>{
                            this.popups[2].fields[0].input_value = event.nativeEvent.text;
                            this.setState({})
                        },
                        focusHandler : (event)=>{
    
                        }
                    },
                    {
                        name : "profiles",
                        profiles_title : "Comptes ajoutés :",
                        profiles_profiles : [
    
                        ],
                        profiles_can_delete : true,
                        deleteHandler : (evt, index)=>{
                            this.popups[2].fields[1].profiles_profiles.splice(index, 1);
                            this.setState({});
                        }
                    }
                ],
                button_title : "Valider",
                pressHandler:(evt)=>{
                    const folder = this.state.folder;
                    this.popups[0].fields[2].profiles_profiles = [...this.popups[2].fields[1].profiles_profiles];
                    folder.profiles = this.popups[0].fields[2].profiles_profiles;
                    this.popups[2].fields[0].input_value = "";
                    Keyboard.dismiss();
                    this.setState({folder : folder, display_popup : 0});
                }
            },
            {
                closeHandler : ()=>{
                    this.popups[3].fields[0].input_value = "";
                    this.popups[3].fields[1].keywords_keywords = [];
                    this.setState({display_popup : 0});
                    Keyboard.dismiss();
                },
                title : "Mots clés associés",
                fields : [
                    {
                        name : "input",
                        input_name : "keyword",
                        input_icon : "",
                        input_value : "",
                        input_opt_icon : {
                            name : "plus",
                            color : "#1599ff",
                            width : 20,
                            height : 20,
                            pressHandler:(event)=>{
                                const new_keywords = this.popups[3].fields[0].input_value.split(",");
                                new_keywords.forEach(new_keyword=>{
                                    if(this.popups[3].fields[1].keywords_keywords.find(keyword=>{return keyword == new_keyword.toLowerCase().trim()}) === undefined){
                                        this.popups[3].fields[1].keywords_keywords.push(new_keyword.toLowerCase().trim());
                                    }
                                });
                                this.popups[3].fields[0].input_value = "";
                                this.setState({});
                            }
                        },
                        input_rounded : false,
                        input_label : "Ajouter un mot clé",
                        input_secure : false,
                        input_can_show : false,
                        inputHandler : (name, event)=>{
                            this.popups[3].fields[0].input_value = event.nativeEvent.text;
                            this.setState({})
                        },
                        focusHandler : (event)=>{
    
                        }
                    },
                    {
                        name : "keywords",
                        keywords_title : "Mots clés ajoutés :",
                        keywords_keywords : [
                        ],
                        keywords_can_delete : true,
                        deleteHandler:(evt, index)=>{
                            this.popups[3].fields[1].keywords_keywords.splice(index, 1);
                            this.setState({});
                        }
                    }
                ],
                button_title : "Valider",
                pressHandler:(evt)=>{
                    Keyboard.dismiss();
                    this.popups[3].fields[0].input_value = "";
                    this.popups[0].fields[3].keywords_keywords = [...this.popups[3].fields[1].keywords_keywords];
                    this.setState({display_popup : 0});
                }
            }
        ];
        
        /**
         * folderPressHandler, function triggered on press event
         * @param {*} event 
         * @param {*} index 
         */
        this.folderPressHandler=(event, index)=>{
            this.props.navigation.navigate('FolderContent',{
                breadcrumb : [index],
                folder : this.folders[index],
            });
        };

        /**
         * pressAddFolder, function triggered on press event
         */
        this.pressAddFolder=()=>{
            this.popups[0].fields[0].input_value = "";
            this.popups[0].fields[2].profiles_profiles = [];
            this.popups[0].fields[3].keywords_keywords = [];
            this.popups[0].button_title = "Créer le dossier";
            this.popups[0].suppr = 0;
            this.popups[0].pressHandler=(evt)=>{
                const folder = {...this.state.folder};
                folder.path=[];
                this.folders.push(folder);
                this.setState({folder : {color : "", title : "", nb_file : 0, profiles : [], keywords : [], sub_folders:[], files:[]}, display_popup : this.state.display_popup -1});
            }
            this.setState({folder : {color : "", title : "", nb_file : 0, profiles : [], keywords : [],  sub_folders:[], files:[]}, display_popup : 0});
        };

        /**
         * pressCamera, function triggered on press event
         */
        this.pressCamera=()=>{
            this.props.navigation.navigate('CameraWindow');
        };

        /**
         * longPressHandler, function triggered on long press event
         * @param {*} event 
         * @param {*} index 
         */
        this.longPressHandler=(event, index)=>{
            const sel_folder = {...this.folders[index]};
            this.popups[0].fields[0].input_value = sel_folder.title;
            if(this.popups[0].fields[1].colors_colors.find(color=>color == sel_folder.color) == undefined){
                this.popups[0].fields[1].colors_colors.push(sel_folder.color);
            }
            this.popups[0].fields[2].profiles_profiles = [...sel_folder.profiles];
            this.popups[0].fields[3].keywords_keywords = [...sel_folder.keywords];
            this.popups[0].button_title = "Modifier le dossier";
            this.popups[0].suppr = 1;
            this.popups[0].supprHandler=(evt)=>{
                this.folders.splice(index, 1);
                this.setState({folder : {color : "", title : "", nb_file : 0, profiles : [], keywords : [], sub_folders:[], files:[]}, display_popup : this.state.display_popup -1});
            };
            this.popups[0].pressHandler=(evt)=>{
                this.folders[index] = {...sel_folder};
                this.setState({folder : {color : "", title : "", nb_file : 0, profiles : [], keywords : [], sub_folders:[], files:[]}, display_popup : this.state.display_popup -1});
            }
            this.setState({folder : sel_folder, display_popup : 0});
        };

        /**
         * pressDeadLines, function triggered on press event
         */
         this.pressDeadLines=()=>{
            this.props.navigation.navigate('Deadline');
        };
    }

    render(){
        return(
            <SafeAreaView style={{flex : 1}}>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.display_popup !=-1}
                >
                    <Popup popup={this.popups[this.state.display_popup]} folder={this.state.folder}></Popup>
                </Modal>
                <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
                    <DeadlineAlert pressHandler={this.pressDeadLines.bind(this)}></DeadlineAlert>
                    <RecentFileCaroussel></RecentFileCaroussel>
                    <FoldersList folders={this.folders} pressHandler={this.folderPressHandler.bind(this)} longPressHandler={this.longPressHandler.bind(this)} addFolder={this.pressAddFolder}></FoldersList>
                </ScrollView>
                <ShortcutsList pressAddFolder={this.pressAddFolder} pressCamera={this.pressCamera}></ShortcutsList>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        paddingLeft : 25,
        paddingRight : 25,
        paddingTop : 25
    },
})

export default Home;