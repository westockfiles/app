import React from "react";

import { SafeAreaView, StyleSheet, Text, FlatList, Alert } from "react-native";
import facture from "../../src/imgs/facture_free.jpg";
import Pencil from "../../src/icons/pencil.svg";
import CustomTextButton from "../Misc/CustomTextButton";
import DeadlineCard from "./DeadlineCard";
import FlashMessage from "react-native-flash-message";
import { showMessage, hideMessage } from "react-native-flash-message";
import DeadlineData from "./DeadlinesData";

class Deadline extends React.Component {
	state = {
		data: DeadlineData["data"],
		firstDeadline: "",
	};

	//Delete a deadline
	delHandler = (item) => {
		const isId = (element) => element.id == item.id;

		this.state.data.splice(this.state.data.findIndex(isId), 1);
		if (this.state.data.length != 0) {
			this.setState({
				firstDeadline:
					" Prochaine deadline dans : " +
					DeadlineCard.secondesToHour(this.state.data[0].time),
			});
		} else {
			this.setState({
				data: [],
				firstDeadline: "Fiouf ! Aucune deadline",
			});
		}

		showMessage({
			message: "Suppression effectuée avec succès !",
			type: "success",
		});
	};
	//Used by the Flatlist component to render the list of deadlines
	renderItem = ({ item }) => (
		<DeadlineCard
			createAlert={this.createAlert}
			id={item.id}
			temps={item.time}
			description={item.description}
		></DeadlineCard>
	);
	//Create the alert before deleting a deadline
	createAlert = (item) =>
		Alert.alert("Suppression", "Voulez vous supprimer la deadline ?", [
			{
				text: "Annuler",
				onPress: () => console.log("Cancel Pressed"),
				style: "cancel",
			},
			{ text: "OK", onPress: () => this.delHandler(item) },
		]);
	//Set the intial data
	componentDidMount() {
		if (this.state.data[0] != undefined) {
			//Order the deadlines
			this.setState({
				data: this.state.data.sort((a, b) => {
					return a.time - b.time;
				}),
			});
			this.setState({
				firstDeadline:
					" Prochaine deadlines dans : " +
					DeadlineCard.secondesToHour(this.state.data[0].time),
			});
		} else {
			this.setState({
				firstDeadline: "Fiouf ! Aucune deadlines",
			});
		}
	}
	render() {
		return (
			<SafeAreaView style={styles.container}>
				<SafeAreaView style={styles.nextDeadline}>
					<Text style={styles.DeadlineTitle}>{this.state.firstDeadline}</Text>
				</SafeAreaView>

				<FlatList
					data={this.state.data}
					renderItem={this.renderItem}
					keyExtractor={(item) => item.time}
				/>
				<SafeAreaView style={styles.spacer}></SafeAreaView>
				<FlashMessage position="top" />
			</SafeAreaView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	nextDeadline: {
		backgroundColor: "#FF5D55",
		borderBottomLeftRadius: 25,
		borderBottomRightRadius: 25,
	},
	DeadlineTitle: {
		margin: 15,
		color: "white",
		fontSize: 20,
		textAlign: "center",
	},
	bold: {
		fontWeight: "bold",
	},
	body: {
		flex: 1,
		paddingLeft: 25,
		paddingRight: 25,
	},
	fields: {
		flex: 0.4,
		justifyContent: "space-between",
		//backgroundColor : "red"
	},
	title: {
		fontWeight: "bold",
		color: "#000",
		fontSize: 20,
		marginBottom: 30,
	},
	item: {
		backgroundColor: "white",
		flexDirection: "row",
		margin: 30,
		padding: 15,
		borderTopColor: "blue",
		borderTopLeftRadius: 0,
		borderTopRightRadius: 0,
		borderRadius: 10,
		borderTopWidth: 5,
		borderTopEndRadius: 5,
		borderTopStartRadius: 5,
	},
	img: {
		borderRadius: 25,
		width: 75,
		height: 100,
	},
	item_title: {
		fontWeight: "bold",
		fontSize: 14,
	},
	item_text: {},
	item_body: {
		paddingLeft: 5,
		flexDirection: "column",
		alignContent: "center",

		flexShrink: 1,
	},
	item_infos: {
		flex: 1,
		flexDirection: "column",
	},
	item_btns: {
		flexDirection: "row",

		flex: 1,
		justifyContent: "space-between",
		alignItems: "center",
	},
	item_img: {
		width: 10,
		height: 10,
		right: 0,
	},

	spacer: {
		flex: 0.2,
	},
});

export default Deadline;
