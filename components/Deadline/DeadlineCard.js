import React from "react";

import { SafeAreaView, StyleSheet, Text, Image, View } from "react-native";
import facture from "../../src/imgs/facture_free.jpg";
import Pencil from "../../src/icons/pencil.svg";
import Cross from "../../src/icons/cross.svg";
import CustomTextButton from "../Misc/CustomTextButton";
import CustomIconButton from "../Misc/CustomIconButton";
import { concat, min } from "react-native-reanimated";

class DeadlineCard extends React.Component {
	// state = {
	//     email : "",
	//     password : "",
	//     state_email : "",
	//     state_password : ""
	// }

	// profiles = {
	//     profile1: require('./profile1.jpg'),
	//     profile2: require('./profile2.jpg'),
	//     profile3: require('./profile3.jpg')
	// }

	// buttonHandler = (event)=>{

	// }
	constructor(props) {
		super(props);
		this.temps = this.props.temps;
		this.id = this.props.id;
		this.description = this.props.description;
	}
	colorItem = (time) => {
		if (time < 172800) {
			//2 jours
			return styles.redBorder;
		} else if (time < 604800) {
			return styles.orangeBorder;
		} else {
			return styles.greenBorder;
		}
	};

	colorBtn = (time) => {
		if (time < 172800) {
			//2 jours
			return "red";
		} else if (time < 604800) {
			return "orange";
		} else {
			return "green";
		}
	};
	static secondesToHour = (time) => {
		var hour = parseInt(time / 3600);
		var minutes = parseInt((time - hour * 3600) / 60);

		if (hour <= 0) {
			return minutes + " minutes";
		} else {
			return hour + " h et " + minutes + " minutes";
		}
	};
	delHandler = (e) => {
		this.props.createAlert(this);
	};

	render() {
		return (
			<SafeAreaView style={[styles.item, this.colorItem(this.temps)]}>
				<SafeAreaView style={styles.close}>
					<CustomIconButton
						press={this.delHandler}
						Icon={Cross}
						width={12}
						height={12}
						padding={14}
						shadow={true}
						pressOut={{ color: "#fff", backgroundColor: "#ff5d55" }}
						pressIn={{ color: "#fff", backgroundColor: "#ff6f68" }}
					></CustomIconButton>
				</SafeAreaView>
				<SafeAreaView>
					<Image style={styles.img} source={facture}></Image>
				</SafeAreaView>
				<SafeAreaView style={styles.item_body}>
					<Text style={styles.item_title}>
						Il reste {DeadlineCard.secondesToHour(this.temps)}
					</Text>
					<Text style={styles.item_text}>{this.description}</Text>

					<SafeAreaView style={styles.item_btns}>
						<CustomTextButton
							color={this.colorBtn(this.temps)}
							invert="1"
							title="Voir le doc"
						></CustomTextButton>
						<Pencil style={styles.item_img}></Pencil>
					</SafeAreaView>
				</SafeAreaView>
			</SafeAreaView>
		);
	}
}

const styles = StyleSheet.create({
	greenBorder: {
		borderTopColor: "#1EA832",
	},
	orangeBorder: {
		borderTopColor: "#FFAC48",
	},
	redBorder: {
		borderTopColor: "#FF5D55",
	},

	container: {
		flex: 1,
	},
	nextDeadline: {
		backgroundColor: "#FF5D55",
		borderBottomLeftRadius: 25,
		borderBottomRightRadius: 25,
	},
	DeadlineTitle: {
		margin: 15,
		color: "white",
		fontSize: 20,
		textAlign: "center",
	},
	bold: {
		fontWeight: "bold",
	},
	body: {
		flex: 1,

		alignSelf: "center",
		justifyContent: "center",
		flexDirection: "column",
	},
	fields: {
		flex: 0.4,
		justifyContent: "space-between",
		//backgroundColor : "red"
	},
	title: {
		fontWeight: "bold",
		color: "#000",
		fontSize: 20,
		marginBottom: 30,
	},
	item: {
		backgroundColor: "white",
		flexDirection: "row",
		margin: 15,
		padding: 15,
		borderTopColor: "blue",
		borderTopLeftRadius: 0,
		borderTopRightRadius: 0,
		borderRadius: 10,
		borderTopWidth: 5,
		borderTopEndRadius: 5,
		borderTopStartRadius: 5,
	},
	img: {
		borderRadius: 15,
		width: 75,
		height: 100,
		flex: 1,
	},
	item_title: {
		fontWeight: "bold",
		fontSize: 14,
	},
	item_text: {
		marginTop: 10,
		marginBottom: 14,
	},
	item_body: {
		paddingLeft: 5,
		flexDirection: "column",
		alignContent: "center",

		flexShrink: 1,
	},
	item_infos: {
		flex: 1,
		flexDirection: "column",
	},
	item_btns: {
		flexDirection: "row",

		flex: 1,
		justifyContent: "space-between",
		alignItems: "center",
	},
	item_img: {
		width: 10,
		height: 10,
		right: 0,
	},

	spacer: {
		flex: 0.2,
	},
	close: {
		position: "absolute",
		alignSelf: "flex-end",
		top: -17,
		right: -10,
	},
});

export default DeadlineCard;
