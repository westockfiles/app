import React from "react"
import { SafeAreaView, StyleSheet, Text } from "react-native"
import InputField from "../Misc/InputField"
import CustomTextButton from "../Misc/CustomTextButton"
import Pressable from "react-native/Libraries/Components/Pressable/Pressable"

/**
 * Class NewPassForm
 * props :
 *  - navigation, navigation element
 */
class NewPassForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            email_username : ""
        };
        /**
         * inputHandler, function triggered on input change event
         * @param {*} name 
         * @param {*} event 
         */
        this.inputHandler = (name, event) =>{
            this.setState({[name] : event.nativeEvent.text});
        };

        /**
         * buttonHandler, function triggered on press event
         * @param {*} event 
         */
        this.buttonHandler = (event) =>{
            this.props.navigation.navigate("ChangePassForm");
        };
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <SafeAreaView style={styles.body}>
                    <Text style={styles.title}>Mot de passe oublié ?</Text>
                    <Text>Veuillez saisir votre nom d'utilisateur ou votre adresse e-mail. Vous recevrez un lien par e-mail pour créer un nouveau mot de passe</Text>
                    <InputField name="email_username" icon="" rounded={false} state="" value={this.state.email_username} label="Email ou nom d'utilisateur" inputHandler={this.inputHandler.bind(this)} focusHandler={(event)=>{}} secure={false} canShow={false}></InputField>
                    <CustomTextButton title="Réinitialiser votre mot de passe" invert={true} padTop={15} padBot={15} handler={this.buttonHandler.bind(this)}></CustomTextButton>
                    <SafeAreaView style={{flexDirection : "row", alignItems:"center"}}>
                        <Text style={styles.log_in_f}>Déjà un email et un mot de passe ?</Text><Pressable onPress={()=>this.props.navigation.navigate("LoginForm")}><Text style={styles.blue}>Se connecter</Text></Pressable>
                    </SafeAreaView>
                </SafeAreaView>
                <SafeAreaView style={styles.spacer}></SafeAreaView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    body:{
        flex:1,
        paddingLeft : 25,
        paddingRight : 25,
        justifyContent:"space-around",
    },
    blue:{
        color : "#1590ff",
        marginLeft : 5
    },
    title:{
        fontWeight : "bold",
        color : "#000",
        fontSize : 26,
        marginTop : 20
    },
    log_in_f:{
        fontSize : 13,
    },
    spacer:{
        flex:0.80
    },
})

export default NewPassForm