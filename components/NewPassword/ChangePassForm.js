import React from "react"
import { SafeAreaView, StyleSheet, Text } from "react-native"
import InputField from "../Misc/InputField"
import CustomTextButton from "../Misc/CustomTextButton"
import ErrorsBubble from "../Misc/ErrorsBubble"

/**
 * Class ChangePassForm
 * props :
 *  - 
 */
class ChangePassForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            password : "",
            checkPassword : "",
    
            state_password : "",
            state_checked_password : "",
    
            error_bubble : false,
            errors_password:{
                nb_car : true,
                maj_min : true,
                nb_fig : true,
                spe_car : true
            }
        };

        /**
         * inputHandler, function triggered on input change event
         * @param {*} name 
         * @param {*} event 
         */
        this.inputHandler = (name, event) =>{
            if(name == "password"){
                let all_is_ok = true;
                if(event.nativeEvent.text.search(/\s+/g) != -1){
                    all_is_ok = false;
                }else{
                    const errors = {
                        nb_car : false,
                        maj_min : false,
                        nb_fig : false,
                        spe_car : false
                    };
                    if(event.nativeEvent.text.search(/^.{12,}/g) == -1){
                        errors.nb_car = true;
                        all_is_ok = false;
                    }
                    if(event.nativeEvent.text.search(/[a-z]+/g) == -1 || event.nativeEvent.text.search(/[A-Z]+/g) == -1){
                        errors.maj_min = true;
                        all_is_ok = false;
                    }
                    if(event.nativeEvent.text.search(/[0-9]+/g) == -1){
                        errors.nb_fig = true;
                        all_is_ok = false;
                    }
                    if(event.nativeEvent.text.search(/[€&#_^@°$£*%!§~]+/g) == -1){
                        errors.spe_car = true;
                        all_is_ok = false;
                    }
                    this.setState({errors_password : errors});
                }
                this.setState({[`state_${name}`] : all_is_ok ? "valid" : "cross"});
            }else{
                if(this.state.password == event.nativeEvent.text){
                    this.setState({state_checked_password : "valid"});
                }else{
                    this.setState({state_checked_password : "cross"});
                }
            }
            this.setState({[name] : event.nativeEvent.text});
        }

        /**
         * buttonhandler, function triggered on press event
         * @param {*} event 
         * @returns 
         */
        this.buttonHandler = (event) =>{
            let all_is_ok = true;
            const states = {
                state_password : "valid",
                state_checked_password : "valid",
            };
            if(this.state.state_password != "valid"){
                states.state_password = "cross";
                all_is_ok = false;
            }
            if(this.state.state_checked_password != "valid"){
                states.state_checked_password = "cross";
                all_is_ok = false;
            }
            if(all_is_ok){
                console.log("Mdp réinit");
                return;
            }
            this.setState({state_password : states.state_password, state_checked_password : states.state_checked_password});
        }

        /**
         * focusHandler, function triggered on focus event
         * @param {*} event 
         */
        this.focusHandler = (event)=>{
            this.setState({error_bubble : event.currentTarget.isFocused()});
        }
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <SafeAreaView></SafeAreaView>
                <SafeAreaView style={styles.body}>
                <Text style={styles.title}>Nouveau mot de passe</Text>
                    {
                        this.state.error_bubble &&
                        <ErrorsBubble style={{top : 0}} fields={[{state : this.state.errors_password.nb_car, text : "12 caractères"}, {state : this.state.errors_password.maj_min, text : "1 lettre majuscule et minuscule"}, {state : this.state.errors_password.nb_fig, text : "1 chiffre"}, {state : this.state.errors_password.spe_car, text : "1 caractère spécial"}]}></ErrorsBubble>
                    }
                    <InputField name="password" icon="" optIcon={{name : "eyeClose", width : 20, height : 20}} rounded={false} state={this.state.state_password} value={this.state.password} label="Mot de passe" inputHandler={this.inputHandler.bind(this)} focusHandler={this.focusHandler.bind(this)} secure={true} canShow={true}></InputField>
                    <InputField name="checkPassword" icon="" optIcon={{name : "eyeClose", width : 20, height : 20}} rounded={false} state={this.state.state_checked_password} value={this.state.checked_password} label="Confirmer le mot de passe" inputHandler={this.inputHandler.bind(this)} focusHandler={(event)=>{}} secure={true} canShow={true}></InputField>
                    <CustomTextButton title="Changer votre mot de passe" invert={true} padTop={15} padBot={15} handler={this.buttonHandler.bind(this)}></CustomTextButton>
                </SafeAreaView>
                <SafeAreaView style={styles.spacer}></SafeAreaView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    body:{
        flex:1,
        justifyContent:"space-around",
        paddingLeft : 25,
        paddingRight : 25,
    },
    blue:{
        color : "#1590ff"
    },
    title:{
        fontWeight : "bold",
        color : "#000",
        fontSize : 26,
        marginTop : 130
    },
    log_in_f:{
        fontSize : 13,
    },
    spacer:{
        flex:0.80
    },
})

export default ChangePassForm