import React from "react"
import { StyleSheet, SafeAreaView, Text, Pressable } from "react-native";
import InputField from "../Misc/InputField"
import CustomTextButton from "../Misc/CustomTextButton"
import CheckBox from "@react-native-community/checkbox";
import ErrorsBubble from "../Misc/ErrorsBubble";

/**
 * Class SiginForm
 * props :
 *  - navigation, navigation element
 */
class SigninForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            email : "",
            username : "",
            password : "",
            check : false,
            checkPassword : "",
    
            state_email : "",
            state_username : "",
            state_password : "",
            state_check : false,
            state_checked_password : "",
    
            error_bubble : false,
            errors_password:{
                nb_car : true,
                maj_min : true,
                nb_fig : true,
                spe_car : true
            }
        };

        /**
         * inputHandler, function triggered on input change event
         * @param {*} name 
         * @param {*} event 
         */
        this.inputHandler = (name, event)=>{
            if(name == "email"){
                if(!event.nativeEvent.text.search(/^([\w-]+[\.]?[\w-]+)[^\.]?@((?:[\w]+\.)+)([a-z]{2,4})/g)){
                    this.setState({[`state_${name}`] : "valid"});
                }else{
                    this.setState({[`state_${name}`] : "cross"});
                }
            }else if(name == "username"){
                if(event.nativeEvent.text.search(/[^\w_-]/g) != -1){
                    this.setState({[`state_${name}`] : "cross"});
                }else{
                    this.setState({[`state_${name}`] : "valid"});
                }
            }else if(name == "password"){
                let all_is_ok = true;
                if(event.nativeEvent.text.search(/\s+/g) != -1){
                    all_is_ok = false;
                }else{
                    const errors = {
                        nb_car : false,
                        maj_min : false,
                        nb_fig : false,
                        spe_car : false
                    };
                    if(event.nativeEvent.text.search(/^.{12,}/g) == -1){
                        errors.nb_car = true;
                        all_is_ok = false;
                    }
                    if(event.nativeEvent.text.search(/[a-z]+/g) == -1 || event.nativeEvent.text.search(/[A-Z]+/g) == -1){
                        errors.maj_min = true;
                        all_is_ok = false;
                    }
                    if(event.nativeEvent.text.search(/[0-9]+/g) == -1){
                        errors.nb_fig = true;
                        all_is_ok = false;
                    }
                    if(event.nativeEvent.text.search(/[€&#_^@°$£*%!§~]+/g) == -1){
                        errors.spe_car = true;
                        all_is_ok = false;
                    }
                    this.setState({errors_password : errors});
                }
                this.setState({[`state_${name}`] : all_is_ok ? "valid" : "cross"});
            }else{
                if(this.state.password == event.nativeEvent.text){
                    this.setState({state_checked_password : "valid"});
                }else{
                    this.setState({state_checked_password : "cross"});
                }
            }
            this.setState({[name] : event.nativeEvent.text});
        };

        /**
         * buttonHandler, function triggered on press event
         * @param {*} event 
         * @returns 
         */
        this.buttonHandler = (event)=>{
            let all_is_ok = true;
            const states = {
                state_email : "valid",
                state_username : "valid",
                state_password : "valid",
                state_check : true,
                state_checked_password : "valid",
            };
            if(this.state.state_email != "valid"){
                states.state_email = "cross";
                all_is_ok = false;
            }
            if(this.state.state_username != "valid"){
                states.state_username = "cross";
                all_is_ok = false;
            }
            if(this.state.state_password != "valid"){
                states.state_password = "cross";
                all_is_ok = false;
            }
            if(!this.state.check){
                states.check = false;
                all_is_ok = false;
            }
            if(this.state.state_checked_password != "valid"){
                states.state_checked_password = "cross";
                all_is_ok = false;
            }
            if(all_is_ok){
                console.log("Compte crée");
                return;
            }
            this.setState({state_email : states.state_email, state_username : states.state_username, state_password : states.state_password, state_check : states.state_check, state_checked_password : states.state_checked_password});
        };

        /**
         * focusHandler, function triggered on focus event
         * @param {*} event 
         */
        this.focusHandler = (event) =>{
            this.setState({error_bubble : event.currentTarget.isFocused()});
        };
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <SafeAreaView style={styles.body}>
                    <Text style={styles.title}>Créer un compte</Text>
                    <InputField name="email" icon="" rounded={false} state={this.state.state_email} value={this.state.email} label="Email" inputHandler={this.inputHandler.bind(this)} focusHandler={(event)=>{}} secure={false} canShow={false}></InputField>
                    <InputField name="username" icon="" rounded={false} state={this.state.state_username} value={this.state.username} label="Nom d'utilisateur" inputHandler={this.inputHandler.bind(this)} focusHandler={(event)=>{}} secure={false} canShow={false}></InputField>
                    {
                        this.state.error_bubble &&
                        <ErrorsBubble style={{top : 60}} fields={[{state : this.state.errors_password.nb_car, text : "12 caractères"}, {state : this.state.errors_password.maj_min, text : "1 lettre majuscule et minuscule"}, {state : this.state.errors_password.nb_fig, text : "1 chiffre"}, {state : this.state.errors_password.spe_car, text : "1 caractère spécial"}]}></ErrorsBubble>
                    }
                    <InputField name="password" icon="" optIcon={{name : "eyeClose", width : 20, height : 20}} rounded={false} state={this.state.state_password} value={this.state.password} label="Mot de passe" inputHandler={this.inputHandler.bind(this)} focusHandler={this.focusHandler.bind(this)} secure={true} canShow={true}></InputField>
                    <InputField name="checkPassword" icon="" optIcon={{name : "eyeClose", width : 20, height : 20}} rounded={false} state={this.state.state_checked_password} value={this.state.checked_password} label="Confirmer le mot de passe" inputHandler={this.inputHandler.bind(this)} focusHandler={(event)=>{}} secure={true} canShow={true}></InputField>
                    <SafeAreaView style={styles.UC}>
                        <CheckBox
                            value={this.state.check}
                            tintColors={{true:"#1599ff" , false: this.state.state_check ? "#ff5d55": ""}}
                            onValueChange={(new_value)=>this.setState({check : new_value, state_check : ""})}
                        ></CheckBox>
                        <Text style={{paddingRight : 15}}>En continuant, j'accepte les <Text style={styles.blue}>conditions d'utilisations</Text> et la <Text style={styles.blue}>politique de confidentialité</Text>.</Text>
                    </SafeAreaView>
                    <CustomTextButton title="Créer mon compte" invert={true} padTop={15} padBot={15} handler={this.buttonHandler.bind(this)}></CustomTextButton>
                </SafeAreaView>
                <Pressable onPress={()=>{this.props.navigation.navigate("LoginForm")}}><Text style={styles.log_in_f}>Se connecter</Text></Pressable>
                <SafeAreaView style={styles.spacer}></SafeAreaView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    body:{
        flex:1,
        paddingLeft : 25,
        paddingRight : 25,
        justifyContent:"space-around",
    },
    UC:{
        flexDirection:"row",
        paddingRight : 10
    },
    blue:{
        color : "#1590ff"
    },
    title:{
        fontWeight : "bold",
        color : "#000",
        fontSize : 26,
        marginTop : 20
    },
    log_in_f:{
        paddingTop : 5,
        alignSelf : "center",
        fontSize : 13,
        color:"#1590ff"
    },
    spacer:{
        flex:0.30
    },
})

export default SigninForm;