import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import CustomIconButton from '../CustomIconButton';
import Valid from '../../../src/icons/valid.svg';

/**
 * Class ColorItem
 * props :
 *  - selected, boolean which indicates if color is selected
 *  - color , color of the item,
 *  - press, function triggered on press event
 */
class ColorItem extends React.Component{
    render(){
        return(
            <SafeAreaView style={styles.container}>
                <CustomIconButton width={10} height={10} padding={10} Icon={Valid} alignSelf={"flex-start"} pressIn={{color : this.props.selected ? "#fff" : this.props.color, backgroundColor : this.props.color}} pressOut={{color : this.props.selected ? "#fff" : this.props.color, backgroundColor : this.props.color}} press={this.props.press} captureColor={true}></CustomIconButton>
            </SafeAreaView>
        )
    }
}

/*
<Pressable>
                <SafeAreaView style={[styles.container, {backgroundColor : this.props.color, width : this.props.width, height : this.props.height}]}>
                </SafeAreaView>
            </Pressable>
*/

const styles = StyleSheet.create({
    container:{
        marginRight : 8,
        marginBottom : 8,
        borderRadius : 25
    }
})

export default ColorItem;