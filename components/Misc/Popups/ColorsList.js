import React from 'react'
import { StyleSheet } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import CustomIconButton from '../CustomIconButton';
import ColorItem from './ColorItem';
import Plus from '../../../src/icons/plus.svg';

/**
 * Class ColorsList
 * props :
 *  - colorSel, color selected,
 *  - press, function triggered on press event,
 *  - getColorSel, function get selected color
 */
class ColorsList extends React.Component{
    constructor(props){
        super(props);
        this.state={
            color_sel : this.props.colorSel
        };
        /**
         * pressHandler, function triggered on press event
         * @param {*} event 
         * @param {*} color 
         */
        this.pressHandler=(event, color)=>{
            this.props.getColorSel(color);
            this.setState({color_sel : color});
        };
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                {
                    this.props.colors.map((color, id)=>{
                        return(
                            <ColorItem key={id} color={color} width={25} height={25} selected={color == this.state.color_sel} press={this.pressHandler.bind(this)}></ColorItem>
                        )
                    })
                }
                <CustomIconButton press={this.props.press} Icon={Plus} width={15} height={15} padding={8} shadow={false} pressOut={{color : "#707070", backgroundColor : "#fff"}} pressIn={{color : "#cacaca", backgroundColor : "#fff"}}></CustomIconButton>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex : 1,
        flexDirection : "row",
        alignSelf : "flex-start",
        flexWrap : "wrap",
    }
})

export default ColorsList;