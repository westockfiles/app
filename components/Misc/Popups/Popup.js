import React from "react";
import { SafeAreaView, StyleSheet, Text } from "react-native";
import CustomIconButton from "../CustomIconButton";
import CustomTextButton from "../CustomTextButton";
import InputField from "../InputField";
import Cross from "../../../src/icons/cross.svg";
import ColorsList from "./ColorsList";
import ProfileBubblesList from "../../Home/ProfileBubbles/ProfileBubblesList";
import Plus from "../../../src/icons/plus.svg";
import KeywordsList from "../../Home/Keywords/KeywordsList";
import ColorPicker from "../ColorPicker/ColorPicker";
import RSquareProfileList from "../RoundedSquare/RSquareList";
import {
	Calendar,
	CalendarList,
	Agenda,
	LocaleConfig,
} from "react-native-calendars";

LocaleConfig.locales["fr"] = {
	monthNames: [
		"Janvier",
		"Février",
		"Mars",
		"Avril",
		"Mai",
		"Juin",
		"Juillet",
		"Août",
		"Septembre",
		"Octobre",
		"Novembre",
		"Décembre",
	],
	monthNamesShort: [
		"Janv.",
		"Févr.",
		"Mars",
		"Avril",
		"Mai",
		"Juin",
		"Juil.",
		"Août",
		"Sept.",
		"Oct.",
		"Nov.",
		"Déc.",
	],
	dayNames: [
		"Dimanche",
		"Lundi",
		"Mardi",
		"Mercredi",
		"Jeudi",
		"Vendredi",
		"Samedi",
	],
	dayNamesShort: ["Dim.", "Lun.", "Mar.", "Mer.", "Jeu.", "Ven.", "Sam."],
	today: "Aujourd'hui",
};
LocaleConfig.defaultLocale = "fr";

/**
 * Class Popup
 * props :
 *  - popup, popup to display
 */
class Popup extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<SafeAreaView style={styles.container}>
				<SafeAreaView style={styles.mask}></SafeAreaView>
				<SafeAreaView style={styles.popup}>
					<SafeAreaView style={styles.close}>
						<CustomIconButton
							press={this.props.popup.closeHandler}
							Icon={Cross}
							width={12}
							height={12}
							padding={14}
							shadow={true}
							pressOut={{ color: "#fff", backgroundColor: "#ff5d55" }}
							pressIn={{ color: "#fff", backgroundColor: "#ff6f68" }}
						></CustomIconButton>
					</SafeAreaView>
					<Text style={styles.title}>{this.props.popup.title}</Text>
					{this.props.popup.fields.map((field, id) => {
						if (field.name == "input") {
							if (field.input_opt_icon) {
								field.input_opt_icon.pressHandler.bind(this);
							}
							return (
								<SafeAreaView key={id} style={styles.field}>
									<InputField
										name={field.input_name}
										icon={field.input_icon}
										optIcon={field.input_opt_icon}
										rounded={field.input_rounded}
										state={this.state[`${field.input_name}_state`]}
										value={field.input_value}
										label={field.input_label}
										secure={field.input_secure}
										canShow={field.input_can_show}
										inputHandler={field.inputHandler}
										focusHandler={field.focusHandler}
									></InputField>
								</SafeAreaView>
							);
						} else if (field.name == "colors") {
							return (
								<SafeAreaView key={id} style={styles.field}>
									<Text style={{ marginBottom: 10 }}>{field.colors_title}</Text>
									<ColorsList
										colors={field.colors_colors}
										press={field.handlerAdd}
										getColorSel={field.getColorSel}
										colorSel={this.props.folder.color}
									></ColorsList>
								</SafeAreaView>
							);
						} else if (field.name == "profiles") {
							return (
								<SafeAreaView key={id} style={styles.field}>
									<Text style={{ marginBottom: 10 }}>
										{field.profiles_title}
									</Text>
									<SafeAreaView style={styles.sub_field}>
										<ProfileBubblesList
											width={30}
											height={30}
											profiles={field.profiles_profiles}
											canDelete={field.profiles_can_delete}
											deleteHandler={field.deleteHandler}
											marginRight={8}
										></ProfileBubblesList>
										{!field.profiles_can_delete && (
											<CustomIconButton
												press={field.handlerAdd}
												Icon={Plus}
												width={15}
												height={15}
												padding={7}
												shadow={false}
												pressOut={{ color: "#707070", backgroundColor: "#fff" }}
												pressIn={{ color: "#cacaca", backgroundColor: "#fff" }}
											></CustomIconButton>
										)}
									</SafeAreaView>
								</SafeAreaView>
							);
						} else if (field.name == "keywords") {
							return (
								<SafeAreaView key={id} style={styles.field}>
									<Text style={{ marginBottom: 10 }}>
										{field.keywords_title}
									</Text>
									<KeywordsList
										keywords={field.keywords_keywords}
										canDelete={field.keywords_can_delete}
										press={field.handlerAdd}
										deleteHandler={field.deleteHandler}
									></KeywordsList>
								</SafeAreaView>
							);
						} else if (field.name == "colorPicker") {
							return (
								<SafeAreaView key={id} style={styles.field}>
									<ColorPicker
										getColor={field.getColor.bind(this)}
									></ColorPicker>
								</SafeAreaView>
							);
						} else if (field.name == "chooseProfileImg") {
							return (
								<SafeAreaView key={id} style={styles.field}>
									<RSquareProfileList
										images={field.user_profiles_list}
										imgProfileHandler={field.imgProfileHandler}
										currentImg={field.current_profile_img}
									></RSquareProfileList>
								</SafeAreaView>
							);
						} else if (field.name == "calendar") {
							return (
								<Calendar
									scrollEnabled={true}
									enableSwipeMonths={true}
									onDayPress={(day) => {
										this.setState({
											marked_date: {
												[day.dateString]: {
													selected: true,
													marked: true,
													selectedColor: "blue",
												},
											},
										});

										field.saveDate(day);
									}}
									markedDates={this.state.marked_date}
								/>
							);
						}
					})}
					<CustomTextButton
						title={this.props.popup.button_title}
						invert={true}
						padTop={10}
						padBot={10}
						handler={this.props.popup.pressHandler}
					></CustomTextButton>
					{this.props.popup.suppr == 1 && (
						<CustomTextButton
							title={"Supprimer le dossier"}
							style={styles.suppr}
							color="red"
							invert={true}
							padTop={10}
							padBot={10}
							handler={this.props.popup.supprHandler}
						></CustomTextButton>
					)}
				</SafeAreaView>
			</SafeAreaView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		position: "absolute",
		justifyContent: "center",
		alignSelf: "center",
		width: "100%",
		height: "100%",
	},
	popup: {
		position: "absolute",
		width: "85%",
		zIndex: 11,
		elevation: 11,
		alignSelf: "center",
		justifyContent: "space-around",
		backgroundColor: "#fff",
		paddingVertical: 50,
		paddingHorizontal: 40,
		borderRadius: 15,
	},
	title: {
		fontSize: 22,
		fontWeight: "bold",
		color: "#000",
		marginBottom: 20,
	},
	mask: {
		flex: 1,
		width: "100%",
		height: "100%",
		position: "absolute",
		backgroundColor: "#000",
		opacity: 0.5,
		zIndex: 10,
		elevation: 10,
	},
	close: {
		position: "absolute",
		alignSelf: "flex-end",
		top: -12,
		right: -12,
	},
	field: {
		position: "relative",
		marginBottom: 40,
	},
	sub_field: {
		flexDirection: "row",
		alignSelf: "flex-start",
	},
	suppr: {
		marginTop: 20,
	},
});

export default Popup;
