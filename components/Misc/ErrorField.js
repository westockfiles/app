import React from "react"
import { SafeAreaView, StyleSheet, Text } from "react-native";

class ErrorField extends React.Component{

    render(){
        return(
            <SafeAreaView>
                <Text style={styles.error}>{this.props.error}</Text>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    error:{
        color : "red"
    }
})

export default ErrorField;