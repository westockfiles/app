import React from 'react';
import { StyleSheet, Pressable, SafeAreaView, Text, TextInput } from 'react-native';
import SearchIcon from '../../src/icons/search.svg'
import Filter from './Filter';
import SearchResults from './SearchResults';
import folderTree from '../Misc/folderTree';

class SearchBar extends React.Component{
    
    state={
        querry : "",
        folder_state : true,
        file_state : true,
    }

    querry_folder_results = [];
    querry_file_results = [];

    searchQuerry=(current_folder, querry)=>{
        if(this.state.folder_state){
            if(current_folder.title.includes(querry)){
                if(!this.querry_folder_results.some(folder=>folder.title == current_folder.title)){
                    const add_folder = {...current_folder};
                    const split = add_folder.title.split(RegExp(`(${querry})`)).filter(elt=>elt != "");
                    add_folder.split = split;
                    add_folder.bold_index = split.indexOf(querry);
                    this.querry_folder_results.push(add_folder);
                }
            }       
        }
        
        if(this.state.file_state){
            current_folder.files.forEach(file=>{
                if(file.name.includes(querry)){
                    if(!this.querry_file_results.some(file_result=>file_result.name == file.name)){
                        const add_file = {...file};
                        const split = add_file.name.split(RegExp(`(${querry})`)).filter(elt=>elt != "");
                        add_file.split = split;
                        add_file.bold_index = split.indexOf(querry);
                        this.querry_file_results.push(add_file);
                    }
                }
            });
        }
    }

    searchFolder=(current_folder, querry)=>{
        current_folder.sub_folders.forEach(sub_folder=>{
            this.searchQuerry(sub_folder, querry);
            this.searchFolder(sub_folder, querry);
        });
    }

    pressFolder=(evt)=>{
        this.setState({folder_state : !this.state.folder_state});
    }

    pressFile=(evt)=>{
        this.setState({file_state : !this.state.file_state});
    }

    inputHandler=(evt)=>{
        const querry = evt.nativeEvent.text;
        this.querry_folder_results = [];
        this.querry_file_results = [];
        if(evt.nativeEvent.text != ""){
            this.searchFolder(folderTree, querry);
        }
        console.log(this.querry_folder_results);
        this.setState({querry : querry});
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <SafeAreaView style={styles.upper_section}>
                    <SafeAreaView style={styles.search_section}>
                        <SafeAreaView style={styles.search_input}>
                            <SearchIcon style={styles.icon}></SearchIcon>
                            <TextInput
                                name={"querry"}
                                value={this.state.querry}
                                placeholder={"Recherche"}
                                placeholderTextColor={"#c9c9c9"}
                                onChange={this.inputHandler}
                            >
                            </TextInput>
                        </SafeAreaView>
                        <Filter folderState={this.state.folder_state} pressFolder={this.pressFolder} fileState={this.state.file_state} pressFile={this.pressFile}></Filter>
                    </SafeAreaView>
                    <Pressable
                        onPress={this.props.pressHandler}
                        style={{marginLeft : 20}}
                    >
                        <Text style={{paddingVertical : 10}}>Annuler</Text>
                    </Pressable>
                </SafeAreaView>
                {
                    (this.querry_folder_results.length != 0 || this.querry_file_results.length != 0) &&
                    <SearchResults folderResults={this.querry_folder_results} folderFilter={this.state.folder_state} fileResults={this.querry_file_results} fileFilter={this.state.file_state}></SearchResults>
                }
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        marginVertical : 15,
    },
    search_input:{
        width : "100%",
        flexDirection : "row",
        alignItems : "center",
        backgroundColor : "#f5f5f5",
        paddingHorizontal : 15,
        borderRadius : 100,
    },
    search_section:{
        width : "80%",
        flexWrap : "wrap",
        flexDirection : "column",
    },
    icon:{
        marginRight : 10
    },
    upper_section:{
        flexDirection : "row",
        alignItems : "center",
        justifyContent : "space-between"
    }
})

export default SearchBar;