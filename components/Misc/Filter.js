import React from 'react';
import { Pressable, SafeAreaView, StyleSheet, Text } from 'react-native';
import FolderFilter from '../../src/icons/littleFolder.svg';
import FileFilter from '../../src/icons/fileFilter.svg';

class Filter extends React.Component{

    state={
        
    }

    styles = StyleSheet.create({
        container:{
            flexDirection : "row",
            alignItems : "center",
            marginLeft : 15
        },
        filter:{
            marginLeft : 15,
            flexDirection : "row",
            alignItems : "center",
            paddingVertical : 15
        },
        icons:{
            marginRight : 5
        }  
    })

    render(){
        return(
            <SafeAreaView style={this.styles.container}>
                <Text>Filtres :</Text>
                <Pressable
                    style={this.styles.filter}
                    onPress={this.props.pressFolder}
                >
                    <FolderFilter width={15} height={15} style={this.styles.icons} fill={this.props.folderState ? "#1599ff" : "#707070"}></FolderFilter>
                    <Text style={{color : this.props.folderState ? "#1599ff" : "#707070"}}>Dossiers</Text>
                </Pressable>
                <Pressable
                    style={this.styles.filter}
                    onPress={this.props.pressFile}
                >
                    <FileFilter width={15} height={15} style={this.styles.icons} fill={this.props.fileState ? "#1599ff" : "#707070"}></FileFilter>
                    <Text style={{color : this.props.fileState ? "#1599ff" : "#707070"}}>Fichiers</Text>
                </Pressable>
            </SafeAreaView>
        )
    }
}

/*

styles = StyleSheet.create({
        container:{
            position : "absolute",
            width : 460,
            marginLeft : "72%",
            backgroundColor : "#f5f5f5",
            paddingVertical : 15,
            borderRadius : 50,
        },
        filter : {
            flexDirection : "row",
            alignItems : "center",
            justifyContent : "center"
        },
        selection:{
            backgroundColor : "#f5f5f5",
            position : "relative",
            alignItems : "center",
            marginTop : 39,
            marginLeft : -1,
            paddingTop : 10,
            borderBottomLeftRadius : 15,
            borderBottomRightRadius : 15,
            paddingHorizontal : 15,
            paddingBottom : 10
        }
    })

    <SafeAreaView
                style={[this.styles.container, {borderBottomRightRadius : this.state.open ? 15 : 50, borderBottomLeftRadius : this.state.open ? 15 : 50}]}
            >
                <Pressable 
                    style={this.styles.filter}
                    onPress={(evt)=>{
                        this.setState({open : !this.state.open, Icon : this.state.open ? BottomArrow : TopArrow});
                    }}
                >
                    <SafeAreaView style={{flexDirection : "row", alignItems : "center"}}>
                        <SafeAreaView style={{marginRight : 5}}>
                            <FolderFilter width={15} height={15} fill={"#707070"}></FolderFilter>
                        </SafeAreaView>
                        <Text style={{marginRight : 5}}>+</Text>
                        <SafeAreaView style={{marginRight : 5}}>
                            <FileFilter width={15} height={15} fill={"#707070"}></FileFilter>
                        </SafeAreaView>
                        <SafeAreaView>
                            <this.state.Icon width={10} height={10}></this.state.Icon>
                        </SafeAreaView>
                    </SafeAreaView>
                </Pressable>
                {
                    this.state.open &&
                    <SafeAreaView style={this.styles.selection}>
                        <Pressable
                            style={{flexDirection : "row", borderTopWidth : 1, paddingTop : 10, borderColor : "#707070"}}
                            onPress={(evt)=>{
                                this.setState({folder_state : !this.state.folder_state});
                            }}
                        >
                            <FolderFilter style={{alignSelf : "center", marginRight : 5}} width={15} height={15} fill={this.state.folder_state ? "#1599ff" : "#707070"}></FolderFilter>
                            <Text style={{fontSize : 12, color : this.state.folder_state ? "#1599ff" : "#707070"}}>Dossiers</Text>
                        </Pressable>
                        <Pressable
                            style={{flexDirection : "row", marginTop : 15, borderTopWidth : 1, paddingTop : 15, borderColor : "#cacaca"}}
                            onPress={(evt)=>{
                                console.log("ok");
                                this.setState({file_state : !this.state.file_state});
                            }}
                        >
                            <FileFilter style={{alignSelf : "center", marginRight : 5}} width={15} height={15} fill={this.state.file_state ? "#1599ff" : "#707070"}></FileFilter>
                            <Text style={{fontSize : 12, color : this.state.file_state ? "#1599ff" : "#707070"}}>Fichiers</Text>
                        </Pressable>
                    </SafeAreaView>
                }
            </SafeAreaView>

*/
export default Filter;