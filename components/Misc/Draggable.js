import React from 'react';
import { Animated, PanResponder, SafeAreaView, StyleSheet } from 'react-native';

/**
 * Class Draggable
 * props :
 *  - mode, mode color wheel or brightness
 *  - width : width of drag
 *  - height : height of drag
 *  - offsetX : offset on x
 *  - offsetY : offset on Y
 *  - colorHandler, function triggered on move event
 *  - px : pos x of parent element on the page
 */
class Draggable extends React.Component{
    constructor(props){
        super(props);
        this.state={
            containerWidth : 0,
            containerHeight : 0,
            pan : new Animated.ValueXY()
        };

        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder : ()=>true,
            onPanResponderGrant : (evt, gestureState)=>{
                if(this.props.mode == "colorWheel"){
                    this.state.pan.setOffset({
                        x : this.state.pan.x._value + (evt.nativeEvent.locationX - this.props.width/2),
                        y : this.state.pan.y._value + (evt.nativeEvent.locationY - this.props.height/2)
                    });
                }else if(this.props.mode == "brightness"){
                    this.state.pan.setOffset({
                        x : this.state.pan.x._value + (evt.nativeEvent.locationX - this.props.width/2),
                        y : this.state.pan.y._value
                    })
                }
                
                this.state.pan.setValue({x : 0, y : 0});
            },
            onPanResponderMove : (evt, gestureState)=>{
                const newdx = gestureState.dx;
                if(this.props.mode == "colorWheel"){
                    const newdy = gestureState.dy;
                    if(Math.abs(evt.nativeEvent.pageX - this.state.containerWidth + this.props.offsetX) + Math.abs(evt.nativeEvent.pageY - this.state.containerHeight + this.props.offsetY -120) <= this.state.containerWidth/2
                    ||
                    Math.abs(evt.nativeEvent.pageX - this.state.containerWidth + this.props.offsetX) + Math.abs(evt.nativeEvent.pageY - this.state.containerHeight - this.props.offsetY - 120) <= this.state.containerWidth/2
                    ||
                    Math.abs(evt.nativeEvent.pageX - this.state.containerWidth - this.props.offsetX) + Math.abs(evt.nativeEvent.pageY - this.state.containerHeight + this.props.offsetY -120) <= this.state.containerWidth/2
                    ||
                    Math.abs(evt.nativeEvent.pageX - this.state.containerWidth - this.props.offsetX) + Math.abs(evt.nativeEvent.pageY - this.state.containerHeight - this.props.offsetY - 120) <= this.state.containerWidth/2){
                        Animated.event([
                            null,
                            {
                                dx : this.state.pan.x,
                                dy : this.state.pan.y
                            }
                        ],{useNativeDriver : false})(evt, {dx: newdx, dy: newdy});
                        this.props.colorHandler(evt.nativeEvent.pageX - this.state.containerWidth + this.props.offsetX, evt.nativeEvent.pageY - this.state.containerHeight + this.props.offsetY - 120);
                    }
                }else if(this.props.mode == "brightness"){
                    if(evt.nativeEvent.pageX - this.props.offsetX >= this.props.px + this.props.offsetX + this.props.radiusOffset && evt.nativeEvent.pageX - this.props.offsetX<= this.state.containerWidth + this.props.px - this.props.offsetX + this.props.radiusOffset){
                        Animated.event([
                            null,
                            {
                                dx : this.state.pan.x
                            }
                        ],{useNativeDriver : false})(evt, {dx: newdx});
                    }
                }
                
            },
            onPanResponderRelease :(evt, gestureState)=>{
                this.state.pan.flattenOffset();
                if(this.props.mode == "brightness"){
                    this.props.sliderHandler((evt.nativeEvent.pageX - this.props.offsetX - this.props.px)/this.state.containerWidth);
                }
            },
        });
    
        /**
         * layoutHandler, function triggered on display
         * @param {*} event 
         */
        this.layoutHandler = (event) =>{
            if(this.props.mode == "colorWheel"){
                this.setState({pan : new Animated.ValueXY({x : event.nativeEvent.layout.width/2- this.props.offsetX, y : event.nativeEvent.layout.height/2- this.props.offsetY}) , containerWidth : event.nativeEvent.layout.width, containerHeight : event.nativeEvent.layout.height});
            }else if(this.props.mode == "brightness"){
                this.setState({pan : new Animated.ValueXY({x : event.nativeEvent.layout.width - this.props.offsetX - this.props.radiusOffset, y : event.nativeEvent.layout.height/2 - this.props.offsetY}), containerWidth : event.nativeEvent.layout.width, containerHeight : event.nativeEvent.layout.height})
            }
        };
    }

    render(){
        return(
            <SafeAreaView style={{flex : 1, width : "100%", height : "100%", position : "absolute", zIndex : 10}} onLayout={this.layoutHandler}>
                <Animated.View style={[styles.drag,{width : this.props.width, height : this.props.height, borderWidth : this.props.borderWidth, transform:[{translateX : this.state.pan.x}, {translateY : this.state.pan.y}]}]} {...this.panResponder.panHandlers}>
                </Animated.View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    drag:{
        borderRadius : 20,
        borderColor : "#707070",
        backgroundColor : "transparent"
    }
})

export default Draggable;