import React from 'react';
import { Pressable, SafeAreaView, StyleSheet, Text } from 'react-native';
import FolderIcon from '../../../src/icons/littleFolder.svg';
import folderTree from '../folderTree';

/**
 * Class FolderResultsItem
 * props : 
 *  - areBorder, boolean indicates if there are border,
 *  - folder, folder element
 *  - navigation, navigation element
 */
class FolderResultsItem extends React.Component{
    constructor(props){
        super(props);

        this.find = false;
        /**
         * buildBreadcrumb, recursivly build the breadcrumb
         * @param {*} current_folder 
         * @param {*} target_title 
         * @param {*} target_path 
         * @returns 
         */
        this.buildBreadcrumb=(current_folder, target_title, target_path)=>{
            if(target_path.length == 0){
                return [current_folder.sub_folders.findIndex(elt=>elt.title == target_title)]
            }else{
                const next_folder = current_folder.sub_folders.find(elt=>elt.title == target_path[0].title);
                const i = current_folder.sub_folders.findIndex(elt=>elt.title == next_folder.title);
                return [...this.buildBreadcrumb(next_folder, target_title, target_path.splice(0, target_path.length-1)), i];
            }
        }
    }
    render(){
        return(
            <SafeAreaView>
                <Pressable
                    style={[styles.container, { borderBottomWidth : this.props.areBorder ? 1 :0 }]}
                    onPress={(evt)=>{
                        this.props.resetSearch();
                        this.props.navigation.navigate('FolderContent',{
                            breadcrumb : this.buildBreadcrumb(folderTree, this.props.folder.title, this.props.folder.path),
                            folder : this.props.folder,
                        });
                    }}
                >
                    <FolderIcon width={30} height={30} fill={this.props.folder.color}></FolderIcon>
                    <SafeAreaView style={styles.text}>
                        {
                            this.props.folder.path.map((path, id)=>{
                                return <Text key={id}>{path}/</Text>
                            })
                        }
                        {
                            this.props.folder.split.map((text, id)=>{
                                return <Text key={id} style={id == this.props.folder.bold_index ? [styles.bold, styles.title] : styles.title}>{text}</Text>
                            })
                        }
                    </SafeAreaView>
                </Pressable>
            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        paddingVertical : 15,
        flexDirection : "row",
        alignItems : "center",
        borderColor : "#c9c9c9",
    },
    text:{
        marginLeft : 10,
        marginTop : 5,
        flexDirection : "row"
    },
    bold:{
        color : "#000",
        fontWeight : "bold"
    },
    title:{
        fontSize : 14
    }
})

export default FolderResultsItem;