import React from 'react';
import { SafeAreaView } from 'react-native';
import FolderResultsItem from './FolderResultsItem';

/**
 * Class FolderResultsList
 * props :
 *  - folders, list of folder,
 *  - areFiles, indeicates if there are files
 */
class FolderResultsList extends React.Component{
    render(){
        return(
            <SafeAreaView>
                {
                    this.props.folders.map((folder, id)=>{
                        return <FolderResultsItem navigation={this.props.navigation} key={id} folder={folder} areBorder={id != this.props.folders.length-1 || this.props.areFiles} resetSearch={this.props.resetSearch}></FolderResultsItem>
                    })
                }
            </SafeAreaView>
        )
    }
}

export default FolderResultsList;