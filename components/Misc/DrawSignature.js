import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';

import { SketchCanvas } from '@terrylinla/react-native-sketch-canvas';

/**
 * Class DrawSignature
 */
class DrawSignature extends React.Component{
    render(){
        return(
            <SafeAreaView style={[styles.container, { borderBottomWidth : this.props.areBorder ? 1 :0 }]}>
                <SketchCanvas style={styles.canva} strokeColor={'black'} strokeWidth={2}>

                </SketchCanvas>
            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        height: 200,
        backgroundColor: 'red'
    },

    canva:{
        flex:1,
    }

})

export default DrawSignature;