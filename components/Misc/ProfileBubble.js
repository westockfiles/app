import React from 'react'
import { SafeAreaView, StyleSheet, Image } from 'react-native';

class ProfileBubble extends React.Component{

    styles = StyleSheet.create({
        img:{
            position : 'relative',
            borderRadius : 25,
            width : this.props.width,
            height : this.props.height
        }
    })

    render(){
        return(
            <SafeAreaView>
                <Image style={this.styles.img} source={this.props.src}></Image>
            </SafeAreaView>
        )
    }
}

export default ProfileBubble;