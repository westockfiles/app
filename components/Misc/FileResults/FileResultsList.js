import React from 'react';
import { SafeAreaView } from 'react-native';
import FileResultsItem from './FileResultsItem';

/**
 * Class FileResultsList
 * props :
 *  - files, list of file
 */
class FileResultsList extends React.Component{    
    render(){
        return(
            <SafeAreaView>
                {
                    this.props.files.map((file, id)=>{
                        return(
                            <FileResultsItem key={id} file={file} areBorder={id != this.props.files.length-1}></FileResultsItem>
                        )
                    })
                }
            </SafeAreaView>
        )
    }
}

export default FileResultsList;