import React from 'react';
import { SafeAreaView, StyleSheet, Text } from 'react-native';
import FileFilter from '../../../src/icons/fileFilter.svg';

/**
 * Class FileResultsItem
 * props :
 *  - areBorder, boolean indicates if there are border
 *  - file, file element
 */
class FileResultsItem extends React.Component{
    render(){
        return(
            <SafeAreaView style={[styles.container, { borderBottomWidth : this.props.areBorder ? 1 :0 }]}>
                <FileFilter style={styles.icon} width={30} height={30} fill={"#707070"}></FileFilter>
                {
                    this.props.file.split.map((text, id)=>{
                        return <Text key={id} style={id == this.props.file.bold_index ? [styles.bold, styles.name] : styles.name}>{text}</Text>
                    })
                }
                <Text style={styles.name}>{this.props.file.extension}</Text>
            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
    container:{
        paddingVertical : 15,
        flexDirection : "row",
        alignItems : "center",
        borderColor : "#c9c9c9",
    },
    icon:{
        marginRight : 10,
    },
    name:{
        fontSize : 14,
    },
    bold:{
        color : "#000",
        fontWeight : "bold"
    }
})

export default FileResultsItem;