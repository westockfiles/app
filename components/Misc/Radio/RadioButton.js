import React from 'react'
import { SafeAreaView, StyleSheet, Text, Pressable } from 'react-native'

/**
 * Class RadioButton
 * props :
 *  - selected
 *  - label
 *  - pressHandler
 */
class RadioButton extends React.Component{

    constructor(props){
        super(props);

        this.state={
            is_selected : this.props.selected,
        }
    }
    
    componentDidUpdate(prevProps, prevState){
        if(this.props.selected != prevProps.selected){
            this.setState({is_selected : this.props.selected});
        }
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <Pressable
                    style = {styles.pressable}
                    onPress = {(e)=>this.props.pressHandler(e, this.props.index)}
                >
                    <SafeAreaView style={[styles.radio, this.state.is_selected ? styles.radioSelected : styles.radioUnselected]}/>
                    <Text style={styles.checkBoxLabel}>{this.props.label}</Text>
                </Pressable>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex : 1,
        marginBottom : 10
    },
    pressable:{
        flexDirection : "row",
        alignItems : "center"
    },
    radio:{
        width : 16,
        height : 16,
        borderRadius : 50,
        borderColor : "#acacac",
        marginRight : 10
    },
    radioSelected : {
        borderWidth : 0,
        backgroundColor : "#1590ff"
    },
    radioUnselected:{
        borderWidth : 1,
        backgroundColor : "#fff"
    }
});

export default RadioButton