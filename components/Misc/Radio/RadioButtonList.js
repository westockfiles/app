import React from 'react'
import { SafeAreaView, StyleSheet, Text } from 'react-native'
import RadioButton from "./RadioButton";

/**
 * Class RadioButtonList
 * props :
 *  - changeHandler
 *  - radioButtons
 */
class RadioButtonList extends React.Component{

    constructor(props){
        super(props);

        this.state={
            index_is_selected : 0,
        }

        this.pressHandler=(e, index)=>{
            this.setState({index_is_selected : index});
        }
    }
    
    componentDidUpdate(prevProps, prevState){
        if(prevState.index_is_selected != this.state.index_is_selected){
            this.props.changeHandler(this.state.index_is_selected);
        }
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                {
                    this.props.radioButtons.map(radio_button=>{
                        return(
                            <RadioButton key={radio_button.id} index={radio_button.id} selected={this.state.index_is_selected == radio_button.id} label={radio_button.label} pressHandler={this.pressHandler.bind(this)}></RadioButton>
                        )
                    })
                }
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex : 1
    },
    radio:{
        borderRadius : 50,
        borderColor : "#acacac",
    }
});

export default RadioButtonList