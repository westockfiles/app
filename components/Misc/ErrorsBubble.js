import { reloadAsync } from "expo-updates"
import React from "react"
import { SafeAreaView, StyleSheet, Text } from "react-native"

/**
 * Class ErrorsBubble
 * props :
 *  - fields, field to display
 */
class ErrorsBubble extends React.Component{

    render(){
        return(
            <SafeAreaView style={[styles.container, {top : this.props.style.top}]}>
                <SafeAreaView style={[styles.bubble, styles.blurred1]}>
                    <Text style={styles.title}>Au minimum :</Text>
                    {
                        this.props.fields.map((field, id)=>{
                            return(
                                <SafeAreaView key={id} style={styles.line}>
                                    <SafeAreaView style={[styles.circle, {backgroundColor : field.state ? "#ff5d55" : "#1590ff"}]}>
                                    </SafeAreaView>
                                    <Text>{field.text}</Text>
                                </SafeAreaView>
                            )
                        })
                    }
                </SafeAreaView>
                <SafeAreaView style={[styles.pointer, styles.blurred2]}>
                </SafeAreaView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex : 1,
        flexDirection : "column",
        justifyContent : "center",
        alignItems : "center",
        alignSelf: "center",
        position : "absolute",
        zIndex : 10,
        elevation : 10
    },
    title:{
        fontWeight : "bold",
        color : "#000",
        paddingBottom : 15,
        fontSize : 18
    },
    circle:{
        width : 15,
        height : 15,
        marginRight : 10,
        borderRadius : 25
    },
    line:{
        flexDirection : "row",
        alignItems : "center",
        color: "#000",
        marginBottom : 10
    },
    bubble:{
        position : "relative",
        backgroundColor : "#fff",
        zIndex : 10,
        elevation : 10,
        padding : 15,
        borderRadius : 15
    },
    pointer:{
        width : 40,
        height : 40,
        marginTop : -25,
        position : "relative",
        transform : [{rotate : "45deg"}],
        borderRadius : 15,
        zIndex : 9,
        elevation : 9
    },
    blurred1:{
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 8,
    },
    blurred2:{
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,

        elevation: 4,
    }
})

export default ErrorsBubble;