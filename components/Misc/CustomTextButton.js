import React from "react"
import { Pressable, SafeAreaView, StyleSheet, Text } from "react-native"

/**
 * Class CustomTextButton
 * props :
 *  - padTop : style padding top
 *  - padBot : style padding bottom
 *  - invert, bollean which indicates if color are inverted between blue and none
 *  - handler, function triggered on input change event
 *  - title, title of button
 *  - color, color of the button
 */
class CustomTextButton extends React.Component{
    constructor(props){
       
        super(props);
        
        this.colorBtn(this.props.color);
        this.styles = StyleSheet.create({
         
            button:{
                alignItems : "center",
                borderColor : this.props.invert ? "transparent" : this.color,
                borderWidth : 1,
                borderRadius: 50,
                paddingTop : this.props.padTop ? this.props.padTop : 5,
                paddingBottom : this.props.padBot ? this.props.padBot : 5,
                paddingLeft : 15,
                paddingRight: 15,
                backgroundColor : this.props.invert ? this.color : "transparent",
            },
            pressOutButton:{
                backgroundColor : this.props.invert ? this.color : "#fff",
            },
            pressInButton:{
                backgroundColor : this.colorClicked,
            },
            pressOutText:{
                color : this.props.invert ? "#fff" : this.colorClicked,
            },
            pressInText:{
                color : "#fff",
            }
        });
        this.state={
            styleText : this.styles.pressOutText,
            styleButton : this.styles.pressOutButton
        };
        /**
         * pressIn, function triggered on press in event
         * @param {*} event 
         */
        this.pressIn=(event)=>{
            this.setState({styleText : this.styles.pressInText, styleButton : this.styles.pressInButton});
        }

        /**
         * pressOut, function triggered on press out event
         * @param {*} event 
         */
        this.pressOut=(event)=>{
            this.setState({styleText : this.styles.pressOutText, styleButton : this.styles.pressOutButton});
        }
    }

    colorBtn = (color) => {
        if(color == "green"){
            this.color = "#1EA832"
            this.colorClicked = "#34B046"
        }else if(color == "orange"){
            this.color = "#FFAC48"
            this.colorClicked = "#FFB45A"
        }else if(color == "red"){
            this.color = "#FF5D55"
            this.colorClicked = "#FF6F68"
        }else if(color == "white-0.2"){
            this.color = "rgba(255,255,255,0.3)";
            this.colorClicked = "#rgba(255,255,255,0.2)";
        }else{ //blue
            this.color = "#1590FF"
            this.colorClicked = "#2D9BFF"
        }

    };

    render(){
        return(
            <SafeAreaView style={this.props.style}>
                <Pressable
                    style={[this.styles.button, this.state.styleButton]}
                    onPressIn={this.pressIn}
                    onPressOut={this.pressOut}
                    onPress={this.props.handler}
                    underlayColor={"#1599ff"}
                    >
                    <Text style={[this.state.styleText, {fontWeight : "700"}]}>{this.props.title}</Text>
                </Pressable>
            </SafeAreaView>
        )
    }
}

export default CustomTextButton;