import React from 'react';
import { Pressable, SafeAreaView, StyleSheet, Text } from 'react-native';
import folderTree from './folderTree';

/**
 * Class Breadcrumb
 * props :
 *  - indexex, path of exploration (indexs of folder in tree)
 *  - pressHandler, function triggered on press event
 */
class Breadcrumb extends React.Component{
    constructor(props){
        super(props);
    
        /**
         * findFolderTitle, recursive function to find a folder
         * @param {*} depth 
         * @param {*} current_index 
         * @param {*} current_folder 
         * @returns 
         */
        this.findFolderInfos=(depth, current_index, current_folder)=>{
            if(current_index-1 == depth){
                return [current_folder.color, current_folder.title];
            }else{
                const next_folder = current_folder.sub_folders[this.props.indexes[current_index]]
                return this.findFolderInfos(depth, current_index+1, next_folder);
            }
        };
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                {
                    this.props.indexes.map((index, id)=>{
                        const infos = this.findFolderInfos(id, 0, folderTree);
                        return(
                            <SafeAreaView key={id} style={styles.body}>
                                {
                                    id != 0 &&
                                    <Text style={this.props.indexes.length != id +1 ? {color : infos[0]} : styles.bright}>/</Text>
                                }
                                <Pressable
                                    onPress={this.props.indexes.length != id + 1 ? this.props.pressHandler : ()=>{}}
                                >
                                    <Text key={id} style={this.props.indexes.length != id +1 ? {color : infos[0]} : styles.bright}>{infos[1]}</Text>
                                </Pressable>
                            </SafeAreaView>
                        )
                    })
                }
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flexDirection : "row",
        alignSelf : "flex-start",
        alignContent : "center",
        paddingVertical : 10,
    },
    body:{
        flexDirection : "row"
    },
    bright:{
        color : "#707070"
    }
})

export default Breadcrumb;