import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Draggable from './Draggable';

/**
 * Class CustomSlider
 * props :
 *  - sliderHandler, function triggered on move event
 */
class CustomSlider extends React.Component{
    render(){
        return(
            <SafeAreaView>
                <LinearGradient colors={["#000", '#fff']} style={styles.gradient} start={{x : 0.05, y : 0.05}} end={{x : 0.95, y : 0.95}}>
                    <Draggable width={35} height={35} offsetX={35/2} offsetY={35/2} borderWidth={4} mode="brightness" px={40} radiusOffset={50/4} sliderHandler={this.props.sliderHandler}></Draggable>
                </LinearGradient>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    gradient:{
        height : 35,
        borderWidth : 1,
        borderRadius : 50
    }
})

export default CustomSlider;