import React from "react";
import { Pressable, SafeAreaView, StyleSheet, Text } from "react-native";
import Logo from '../../src/logos/WeStock.svg';
import LeftArrow from '../../src/icons/leftArrow.svg'
import CustomIconButton from "./CustomIconButton";
import CustomTextButton from "./CustomTextButton";
import ProfileBubble from "../Home/ProfileBubbles/ProfileBubble";
import Breadcrumb from "./Breadcrumb";
import SearchBar from "./Search/SearchBar";

/**
 * Class Banner
 * props :
 *  - LeftIcon, icon on the left
 *  - buttonTitle, title of button if exist
 *  - Icon, optionnal icon
 *  - profile, profile connected
 *  - pressIn, style apply on press in event
 *  - pressOutn, style apply on press out and default
 *  - breadcrubm, breadcrumb of navigation
 */
class Banner extends React.Component{
    constructor(props){
        super(props);
        this.state={
            search_mode : false,
        };
    
        this.leftIcons={
            Logo : Logo,
            LeftArrow : LeftArrow
        };
    
        this.LeftIcon = this.leftIcons[this.props.LeftIcon.name];
    
        /**
         * inputHandler, function triggered on input change event
         * @param {*} name 
         * @param {*} evt 
         */
        this.inputHandler=(name, evt)=>{
            this.setState({[name] : evt.nativeEvent.text});
        };

        /**
         * pressSearch, function triggered on press event
         * @param {*} evt 
         */
        this.pressSearch=(evt)=>{
            this.setState({search_mode : !this.state.search_mode});
        };

        /**
         * pressBack, function triggered on press event
         * @param {*} evt 
         */
        this.pressBack=(evt)=>{
            this.setState({search_mode : false});
        };

        /**
         * resetSearch, function triggerred on press event
         * @param {*} evt 
         */
        this.resetSearch=(evt)=>{
            this.setState({search_mode : false});
        }
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <SafeAreaView style={styles.body}>
                    {
                        this.props.LeftIcon.name == "LeftArrow" &&
                        <Pressable
                            onPress={()=>{this.props.press()}}
                            style = {{marginBottom : 20,marginTop : 30, flexDirection : "row", alignItems : "center"}}
                        >
                            <this.LeftIcon fill={this.props.LeftIcon.color} width={20} height={20}></this.LeftIcon>
                            {
                                this.props.title &&
                                <Text style={{fontSize : 18, color : "#000", marginLeft : 10, fontWeight : "bold"}}>{this.props.title}</Text>
                            }
                        </Pressable>
                    }
                    {
                        this.state.search_mode &&
                        <SearchBar resetSearch={this.resetSearch.bind(this)} navigation={this.props.navigation} pressHandler={this.pressBack.bind(this)}></SearchBar>
                    }
                    {
                        !this.state.search_mode && this.props.LeftIcon.name != "LeftArrow" &&
                        <this.LeftIcon fill={this.props.LeftIcon.color} style={styles.logo}></this.LeftIcon>
                    }
                    {
                        this.props.buttonTitle &&
                        <CustomTextButton style={styles.button} title={this.props.buttonTitle} invert={false} handler={this.props.handler}></CustomTextButton>
                    }
                    {
                        !this.state.search_mode && (this.props.Icon || this.props.profile) &&
                        <SafeAreaView style={styles.opt}>
                        {
                            
                            this.props.Icon &&
                            <CustomIconButton Icon={this.props.Icon} width={20} height={20} empty={true} pressIn={this.props.pressIn} pressOut={this.props.pressOut} press={this.pressSearch.bind(this)}></CustomIconButton>
                        }
                        {
                            !this.state.search_mode && this.props.profile &&
                            <Pressable
                                onPress={(e)=>this.props.navigation.navigate("Parameters")}
                            >
                                <ProfileBubble width={31} height={31} src={this.props.profile}></ProfileBubble>
                            </Pressable>
                        }
                        </SafeAreaView>
                    }
                </SafeAreaView>
                {
                    this.props.breadcrumb &&
                    <SafeAreaView style={styles.breadcrumb}>
                        <Breadcrumb indexes={this.props.breadcrumb} pressHandler={this.props.getIndex}></Breadcrumb>
                    </SafeAreaView>
                }
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        zIndex : 4,
        paddingHorizontal : 25,
        backgroundColor: "#fff",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        
        elevation: 10,
    },
    body:{
        flexDirection : "row",
        justifyContent : "space-between",
        alignItems : "center",
    },
    logo:{
        width:170,
        height:80
    },
    opt:{
        flexDirection : 'row',
        alignItems : "center"
    },
    breadcrumb:{
        marginTop : 5,
        borderTopColor : "#707070",
        borderTopWidth : 1,
    }
});

export default Banner;