import React from 'react'
import { SafeAreaView, Pressable, StyleSheet } from "react-native";

/**
 * Class CustomIconButton
 * props :
 *  - pressOut, style on press out event and default
 *  - pressIn, style on press in event
 *  - alignSelf, style alignself to apply,
 *  - padding, style padding to apply,
 *  - captureColor, boolean which indicates if color picker
 *  - press, function triggered on press event,
 *  - shadow, boolean which indicates il are shadows
 *  - blurred, same as shadow for blur,
 *  - empty, same as blurred for empty
 *  - Icon, icon to apply,
 *  - width, icon width,
 *  - height, icon height
 */
class CustomIconButton extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            color : this.props.pressOut.color,
            backgroundColor : this.props.pressOut.backgroundColor,
        };
        this.styles = StyleSheet.create({
            container:{
                alignSelf : this.props.alignSelf ? this.props.alignSelf : "flex-end",
                padding : this.props.padding ? this.props.padding : 20,
                borderRadius : 50,
            },
            blurred:{
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.29,
                shadowRadius: 4.65,
    
                elevation: 7,
            },
            empty:{
                backgroundColor : "#fff"
            }
        });

        /**
         * pressIn, function triggered on press in event
         * @param {*} prevProps 
         */
        this.pressIn=()=>{
            this.setState({color : this.props.pressIn.color, backgroundColor : this.props.pressIn.backgroundColor});
        }

        /**
         * pressOut, function triggered on press out
         */
        this.pressOut=()=>{
            this.setState({color : this.props.pressOut.color, backgroundColor : this.props.pressOut.backgroundColor});
        }
    }

    componentDidUpdate(prevProps){
        if(prevProps.pressOut.color != this.props.pressOut.color){
            this.setState(this.props.pressOut);
        }
    }

    render(){
        return(
            <Pressable
                    onPress={(event)=>this.props.captureColor ? this.props.press(event, this.props.pressOut.backgroundColor) : this.props.press(event)}
                    onPressIn={this.pressIn}
                    onPressOut={this.pressOut}
            >
                <SafeAreaView style={[this.styles.container, {backgroundColor : this.state.backgroundColor}, this.props.shadow ? this.styles.blurred : this.props.empty ? this.styles.empty : { borderWidth : 1, borderColor : this.state.color}]}>
                    <this.props.Icon width={this.props.width} height={this.props.height} fill={this.state.color} secondaryFill={this.state.backgroundColor}></this.props.Icon>
                </SafeAreaView>
            </Pressable>

        )
    }
}

export default CustomIconButton;