const Icons = {};

Icons.letter = require('../../src/icons/letter.svg').default;
Icons.lock = require('../../src/icons/lock.svg').default;
Icons.cross = require('../../src/icons/cross.svg').default;
Icons.valid = require('../../src/icons/valid.svg').default;
Icons.eye = require('../../src/icons/eye.svg').default;
Icons.plus = require('../../src/icons/plus.svg').default;
Icons.eyeClose = require('../../src/icons/eyeClose.svg').default;
Icons.search = require('../../src/icons/search.svg').default;

export default Icons;