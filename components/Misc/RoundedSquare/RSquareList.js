import React from 'react';
import RSquareProfileItem from './RSquareProfileItem';
import { SafeAreaView, StyleSheet } from 'react-native';


class RSquareProfileList extends React.Component{

    render(){

        return(

            <SafeAreaView style={styles.container}>

            {
                this.props.images.map((image, id)=>{
                    return(
                        <RSquareProfileItem key={id} source={image} imgProfileHandler={this.props.imgProfileHandler} isSelected={this.props.currentImg == image}></RSquareProfileItem>
                    )
                })
            }

            </SafeAreaView>
        )
    }

}

const styles = StyleSheet.create({

    container:{
        flex:1,
        justifyContent: 'space-between',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
    },


})

export default RSquareProfileList;