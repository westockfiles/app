import React from 'react';
import { SafeAreaView, StyleSheet, ImageBackground } from 'react-native';
import Pressable from 'react-native/Libraries/Components/Pressable/Pressable';
import profiles from '../../../src/imgs/profiles';


class RSquareProfileItem extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
        return(
            <Pressable onPress={(e) => this.props.imgProfileHandler(e, this.props.source)}>
                <SafeAreaView style={styles.userProfileImgContainer}>
                    <ImageBackground style={this.props.isSelected ? styles.isSelected : styles.userProfileImg} source={profiles[this.props.source]}/>
                </SafeAreaView>
            </Pressable>

        )
    }

}

const styles = StyleSheet.create({

    userProfileImgContainer:{
        borderRadius: 20,
        overflow: 'hidden',
        width: 100,
        aspectRatio: 1,
        marginBottom: 20,
    },

    isSelected:{
        borderRadius: 20,
        overflow: 'hidden',
        width: 100,
        aspectRatio: 1,
        marginBottom: 20,
        borderWidth: 3,
        borderColor: "#1590FF"
    },

    userProfileImg:{
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
    },

})

export default RSquareProfileItem;