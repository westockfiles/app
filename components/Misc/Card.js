import React from 'react';
import { SafeAreaView, StyleSheet, Text, Image } from 'react-native';
import ProfileBubblesList from "../Home/ProfileBubbles/ProfileBubblesList";
import RadioButtonList from '../Misc/Radio/RadioButtonList';
import CustomIconButton from "../Misc/CustomIconButton"
import CustomTextButton from "../Misc/CustomTextButton"

import Plus from "../../src/icons/plus.svg"
import PathComp from '../Paths/PathComp';

/**
 * Class Card
 * props :
 *  
 */
class Card extends React.Component{
    constructor(props){
        super(props);

    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
               
                <Text style={styles.cardTitle}>{this.props.card.title}</Text>
                <SafeAreaView style={styles.cardSeparator}/>
                <SafeAreaView style={styles.cardContent}>
                {
                    this.props.card.fields.map((field, id) => {

                        if(field.name == "profiles"){
                            return(
                            
                                <SafeAreaView key={id} style={styles.profileBubblesList}>
                                    <ProfileBubblesList width={30} height={30} profiles={field.profiles} deleteHandler={()=>{}} marginRight={8}></ProfileBubblesList>
                                    <CustomIconButton press={field.handlerAddUser} Icon={Plus} width={15} height={15} padding={7} shadow={false} pressOut={{color : "#707070", backgroundColor : "#fff"}} pressIn={{color : "#cacaca", backgroundColor : "#fff"}}></CustomIconButton>
                                </SafeAreaView>
                            
                            )
                        }else if(field.name == "notifications"){
                            return(
                                <RadioButtonList key={id} radioButtons={field.radioButtons} changeHandler={field.changeHandler}></RadioButtonList>
                            )
                        }else if(field.name == "signature"){
                            return(
                                <SafeAreaView key={id}>
                                    <Image style={styles.signatureImg} source={field.source}></Image>
                                    <CustomTextButton press={field.handlerChangeSignature} style={styles.changeSignature} pressIn={{color: "#2D9BFF"} } title="Modifier la signature"></CustomTextButton>
                                </SafeAreaView>
                            )
                        }else if(field.name == "path"){
                            return(
            
                                <SafeAreaView key={id} style={styles.path}>
                                    <Text>
                                        <Text style={styles.blue}>{field.path}</Text>
                                        <Text>{field.file_name}</Text>
                                    </Text>
                                </SafeAreaView>
                            )
                            
                        }else if(field.name == "button"){
                            return(
                                <CustomTextButton key={id} padTop={10} padBot={10} invert title={field.textButton}></CustomTextButton>
                            )
                        }else if(field.name == "tree"){
                            return(
                                <PathComp key={id} dataTree={field.data_tree}></PathComp>
                            )
                           
                        }
                        
                    })
                }
                </SafeAreaView>
               
            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
    container:{
        width: "100%",
        marginBottom:20,
        backgroundColor: "#ffffff",
        padding:20,
        borderRadius:18,
        

        shadowColor: "#000",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 5,
    },

    cardTitle:{
        paddingLeft:3,
        paddingRight:3,
        fontSize:18,
        fontWeight: 'bold',
    },

    cardSeparator:{

        width:"100%",
        height:2,
        marginBottom:20,
        marginTop:20,
        backgroundColor: "#ACACAC",

    },

    cardContent:{
        paddingLeft:3,
        paddingRight:3,

        width: "100%",
    },

    profileBubblesList:{
        flexDirection: 'row',
    },

    signatureImg:{
        maxWidth:'100%',
    },

    changeSignature:{
        marginTop: 20,
    },

    //partie d'Owein
    path: {
		alignSelf: "flex-start",
		marginBottom: 15,
	},
	blue: {
		color: "#1590FF",
	},
   
})

export default Card;