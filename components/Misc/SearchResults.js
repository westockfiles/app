import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import FolderResultsList from './FolderResults/FolderResultsList';
import FileResultsList from './FileResults/FileResultsList';

class SearchResults extends React.Component{
    render(){
        return(
            <SafeAreaView style={styles.container}>
                {   
                    this.props.folderFilter &&
                    <FolderResultsList folders={this.props.folderResults} areFiles={this.props.fileResults.length != 0 && this.props.fileFilter}></FolderResultsList>   
                }
                {
                    this.props.fileFilter &&
                    <FileResultsList files={this.props.fileResults}></FileResultsList>
                }
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container :{
        marginVertical : 20,
        borderTopWidth : 1,
        paddingHorizontal : 10,
        borderColor : "#c9c9c9"
    }
})

export default SearchResults;