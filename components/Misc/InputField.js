import React from "react"
import {SafeAreaView, TextInput, StyleSheet, Pressable} from "react-native"
import Icons from "./Icons"

/**
 * Class InputField
 * props :
 *  - icon, icon to display,
 *  - color : color
 *  - optIcon, optionnal icon to display
 *  - secure, boolean indicates if text is in secure mode
 *  - rounded, boolean indicates if textfield is rounded
 *  - name, name of the field
 *  - inputHandler, function triggered on input change event
 *  - focusHandler, function triggered on focus event
 *  - onEndEditing, function triggered on leave focus event
 *  - value, value of the field
 *  - label, label/placeholder of the field
 *  - canShow, boolean indicates if secure field can be show
 */
class InputField extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            color : this.props.state =="cross" ? "#ff5d55" : this.props.state == "valid" ? "#1590ff" : "#c9c9c9",
            icon : Icons[this.props.state],
            optIcon : this.props.optIcon ? Icons[this.props.optIcon.name] : false,
            secure : this.props.secure
        };
        this.Icon = Icons[this.props.icon];
        this.styles = StyleSheet.create({
            container:{
                flexDirection : "row",
                alignItems : "center",
                paddingLeft : this.Icon ? 15 : 0,
                paddingRight : this.props.rounded ? 10 : 0,
                borderWidth : this.props.rounded ? 1 : 0,
                borderBottomWidth : 1,
                borderRadius : this.props.rounded ? 25 : 0,
            },
            input:{
                flex:1
            },
            icon:{
                marginRight : 10,
            },
        });
    }

    componentDidUpdate(prevProps){
        if(prevProps.state != this.props.state){
            this.setState({
                color : this.props.state =="cross" ? "#ff5d55" : this.props.state == "valid" ? "#1590ff" : "#c9c9c9",
                Icon_state : Icons[this.props.state]
            });
        }else if(prevProps.optIcon != this.props.optIcon){
            this.setState({optIcon : this.props.optIcon ? Icons[this.props.optIcon.name] : false});
        }
    }

    render(){
        return(
            <SafeAreaView style = {[this.styles.container, {borderColor : this.state.color}]}>
                {
                    this.Icon &&
                    <this.Icon width="30" height="15" style={this.styles.icon} fill={this.state.color}></this.Icon>
                }
                <TextInput
                    style={this.styles.input}
                    name={this.props.name}
                    onChange={event=>this.props.inputHandler(this.props.name, event)}
                    onFocus = {event=>this.props.focusHandler(event)}
                    onEndEditing = {event=>this.props.focusHandler(event)}
                    value={this.props.value}
                    placeholder={this.props.label}
                    placeholderTextColor={"#c9c9c9"}
                    secureTextEntry={this.state.secure}
                />
                {
                    this.state.Icon_state && !this.props.canShow && 
                    <this.state.Icon_state width="10" height="10" fill={this.state.color}></this.state.Icon_state>
                }
                {
                    this.state.optIcon &&
                    <Pressable onPress={(event)=>{
                        if(this.props.optIcon.name == "eyeClose" || this.props.optIcon.name == "eye"){
                            this.setState({optIcon : this.state.secure ? Icons["eye"] : Icons["eyeClose"], secure : !this.state.secure})
                        }else{
                            this.props.optIcon.pressHandler(event);
                        }
                    }}>
                        <this.state.optIcon width={this.props.optIcon.width} height={this.props.optIcon.height} fill={this.props.optIcon.color ? this.props.optIcon.color : this.state.color}></this.state.optIcon>
                    </Pressable>
                }
            </SafeAreaView>
        );
    }
}

export default InputField;