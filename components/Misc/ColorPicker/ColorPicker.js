import React from 'react'
import { SafeAreaView } from 'react-native';
import ColorWheel from './ColorWheel';
import CustomSlider from '../CustomSlider';

/**
 * Class ColorPicker
 * props :
 *  -
 */
class ColorPicker extends React.Component{
    constructor(props){
        super(props);
        this.state={
            value : 1,
            r : 255,
            g : 255,
            b : 255,
            a : 255
        };

        /**
         * sliderHander, function triggered on press out event
         * @param {*} value 
         */
        this.sliderHandler = (value)=>{
            this.setState({value : value});
        };

        /**
         * colorHandler, function triggered on move event
         * @param {*} r 
         * @param {*} g 
         * @param {*} b 
         * @param {*} a 
         */
        this.colorHandler = (r, g ,b, a) =>{
            this.props.getColor(convertRgbToHex(r,g,b));
            this.setState({r : r, g : g, b : b, a : a});
        };
    }

    render(){
        return(
            <SafeAreaView style={{alignSelf : 'center', width: "100%"}}>
                <ColorWheel value={this.state.value} colorHandler={this.colorHandler.bind(this)}></ColorWheel>
                <CustomSlider sliderHandler={this.sliderHandler.bind(this)}></CustomSlider>
                <SafeAreaView style={{marginTop : 30, width:"100%", height : 50, backgroundColor : `rgba(${this.state.r},${this.state.g},${this.state.b},${this.state.a})`}}></SafeAreaView>
            </SafeAreaView>
        )
    }
}

/**
 * ConvertRgbToHex, convert rgb to hexadecimal value
 * @param {*} r 
 * @param {*} g 
 * @param {*} b 
 * @returns 
 */
function convertRgbToHex(r,g,b){
    const r_converted = r/16;
    const g_converted = g/16;
    const b_converted = b/16;
    const rbg_converted_values = [Math.trunc(r_converted), Math.trunc(parseFloat((r_converted%1).toFixed(4)*16)), Math.trunc(g_converted), Math.trunc(parseFloat((g_converted%1).toFixed(4)*16)), Math.trunc(b_converted), Math.trunc(parseFloat((b_converted%1).toFixed(4)*16))];
    let equivalent = ["a","b","c","d","e","f"];
    let hex = "#";
    rbg_converted_values.forEach(value=>{
        if(value < 10){
            hex+=String(value);
        }else{
            hex+=equivalent[value-10];
        }
    })
    return hex;
}

export default ColorPicker;