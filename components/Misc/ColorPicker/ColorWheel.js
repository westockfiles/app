import React from 'react';
import { PixelRatio, SafeAreaView, StyleSheet} from 'react-native';
import Canvas, { ImageData } from 'react-native-canvas';
import Draggable from '../Draggable';

/**
 * Class ColorWheel
 * props :
 *  -
 */
class ColorWheel extends React.Component{
    constructor(props){
        super(props);
        this.data=[];
        this.canvas = "";
        /**
         * handleCanvas(), create canvas
         * @param {*} canvas 
         */
        this.handleCanvas = (canvas) =>{
            if(canvas != null){
                if(this.canvas == ""){
                    this.canvas = canvas;
                }
                canvas.width = Math.ceil(200*PixelRatio.get());
                canvas.height = Math.ceil(200*PixelRatio.get());
                const ctx = canvas.getContext("2d");
                const radius = canvas.width/2;
                const data = [];
                for(let i=0; i< (canvas.width * canvas.height) * 4; i++){
                    data[i]=0;
                }
                for (let x = -radius; x < radius; x++) {
                    for (let y = -radius; y < radius; y++) {
                        const distance = Math.sqrt(x*x + y*y);
                        const phi = rad2deg(Math.atan2(y, x));
                        const rgb = hsv2rgb(phi, distance/radius, this.props.value);
                        
                        if (distance > radius) {
                            continue;
                        }
                        const rowLength = 2*radius;
                        const adjustedX = x + radius;
                        const adjustedY = y + radius;
                        const pixelWidth = 4;
                        const index = (adjustedX + (adjustedY * rowLength)) * pixelWidth;
                        data[index] = rgb[0];
                        data[index+1] = rgb[1];
                        data[index+2] = rgb[2];
                        data[index+3] = 255;
                    }
                } 
                ctx.putImageData(new ImageData(canvas, data, canvas.height, canvas.width), 0, 0);
                this.data = data;
            }
        };

        /**
         * colorHandler, function triggered on move event
         * @param {*} x 
         * @param {*} y 
         */
        this.colorHandler=(x, y)=>{
            this.getColor(Math.ceil(x*PixelRatio.get()), Math.ceil(y*PixelRatio.get()));
        };

        /**
         * getColor, getColor of a pixel on the wheel
         * @param {*} x 
         * @param {*} y 
         */
        this.getColor = (x, y)=>{
            const radius = Math.ceil(200*PixelRatio.get())/2;
            const rowLength = 2*radius;
            const adjustedX = x + radius;
            const adjustedY = y + radius;
            const pixelWidth = 4;
            const index = (adjustedX + (adjustedY * rowLength)) * pixelWidth;
            this.props.colorHandler(this.data[index],this.data[index+1],this.data[index+2],this.data[index+3])
        };
    }

    componentDidUpdate=(prevProps)=>{
        if(prevProps.value != this.props.value){
            this.handleCanvas(this.canvas);
        }
    }

    render(){
        return(
            <SafeAreaView style={{alignSelf : "center", alignContent : "center", width: 200, height : 200, marginBottom : 20}}>
                <Draggable width={25} height={25} offsetX={25/2} offsetY={25/2} borderWidth={5} mode={"colorWheel"} colorHandler={this.colorHandler} px={10} py={10}></Draggable>
                <Canvas style={{width : "100%", height : "100%", borderRadius : 120}} ref={this.handleCanvas}/>
            </SafeAreaView>
        )
    }
}

/**
 * hsv2rgb, convert hsv value to rgb value
 * @param {*} hue 
 * @param {*} saturation 
 * @param {*} value 
 * @returns 
 */
function hsv2rgb(hue, saturation, value) {
    const chroma = value * saturation;
    const hue1 = hue / 60;
    const x = chroma * (1- Math.abs((hue1 % 2) - 1));
    let r1, g1, b1;
    if (hue1 >= 0 && hue1 <= 1) {
      ([r1, g1, b1] = [chroma, x, 0]);
    } else if (hue1 >= 1 && hue1 <= 2) {
      ([r1, g1, b1] = [x, chroma, 0]);
    } else if (hue1 >= 2 && hue1 <= 3) {
      ([r1, g1, b1] = [0, chroma, x]);
    } else if (hue1 >= 3 && hue1 <= 4) {
      ([r1, g1, b1] = [0, x, chroma]);
    } else if (hue1 >= 4 && hue1 <= 5) {
      ([r1, g1, b1] = [x, 0, chroma]);
    } else if (hue1 >= 5 && hue1 <= 6) {
      ([r1, g1, b1] = [chroma, 0, x]);
    }
    
    const m = value - chroma;
    const [r,g,b] = [r1+m, g1+m, b1+m];
    
    return [255*r,255*g,255*b];
}

/**
 * rad2deg, convert rad to deg
 * @param {*} rad 
 * @returns deg
 */
function rad2deg(rad) {
    return ((rad + Math.PI) / (2 * Math.PI)) * 360;
  }

export default ColorWheel;