const profiles = {
    profile1: require('./profile1.jpg'),
    profile2: require('./profile2.jpg'),
    profile3: require('./profile3.jpg')
}

export default profiles;