import 'react-native-gesture-handler';
import React, { Fragment, useEffect } from 'react';
import { StyleSheet } from 'react-native';
import LoginForm from './components/Login/LoginForm';
import SigninForm from './components/Signin/SigninForm';
import NewPassForm from './components/NewPassword/NewPassForm';
import ChangePassForm from './components/NewPassword/ChangePassForm';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, StackView } from '@react-navigation/stack';
import Home from './components/Home/Home';
import SearchIcon from './src/icons/search.svg';
import Banner from './components/Misc/Banner';
import FolderContent from './components/Home/Folders/FolderContent';
import folderTree from './components/Misc/folderTree';
import Deadline from './components/Deadline/Deadline';
import SplashScreen from 'react-native-splash-screen';
import Parameters from './components/Parameters/Parameters';
import ChoosePathFile from './components/Paths/ChoosePathFile';
import AddDeadline from './components/AddDeadlines/AddDeadline';
import CameraWindow from './components/CameraWindow/CameraWindow';

const Stack = createStackNavigator();
const HomeStack = createStackNavigator();
const FormsStack = createStackNavigator();

let breadcrumb = null;

function findFolder(depth, current_index, current_folder){
  if(current_index-1 == depth){
      console.log(current_folder);
      return current_folder;
  }else{
      const next_folder = current_folder.sub_folders[breadcrumb[current_index]]
      return findFolder(depth, current_index+1, next_folder);
  }
}

function goBack(scene){
  const breadcrumb = scene.scene.route.params.breadcrumb;
  if(breadcrumb.length-1){
    breadcrumb.splice(breadcrumb.length -1, 1);
    scene.navigation.navigate('FolderContent',{
      breadcrumb : breadcrumb,
      folder : findFolder(breadcrumb.length-1, 0, folderTree),
    });
  }else{
    scene.navigation.goBack();
  }
}

function HomeNavigator(){
  return (
    <HomeStack.Navigator initialRouteName="Parameters" screenOptions={{ headerShown: "screen" }}>
      <HomeStack.Screen name="Home" component={Home} options={{header:(scene, previous, navigator)=>{
          return <Banner navigation={scene.navigation} LeftIcon={{name : "Logo", color : "#000"}} Icon={SearchIcon} profile={require('./src/imgs/profile1.jpg')} pressIn={{color : "#707070", backgroundColor : "#fff"}} pressOut={{color : "#707070", backgroundColor : "#fff"}}></Banner>
        }}}></HomeStack.Screen>
      <HomeStack.Screen name="FolderContent" component={FolderContent} options={{header:(scene, previous, navigator)=>{
          breadcrumb = scene.scene.route.params.breadcrumb;
          return <Banner navigation={scene.navigation} breadcrumb = {breadcrumb} LeftIcon={{name : "LeftArrow", color : "#707070"}} Icon={SearchIcon} profile={require('./src/imgs/profile1.jpg')} press={()=>goBack(scene)} pressIn={{color : "#707070", backgroundColor : "#fff"}} pressOut={{color : "#707070", backgroundColor : "#fff"}} getIndex={(evt , index)=>{
            const target_index = evt._targetInst.child.key;
            breadcrumb.splice(target_index+1);
            scene.navigation.navigate('FolderContent',{
              breadcrumb : breadcrumb,
              folder : findFolder(breadcrumb.length-1, 0, folderTree)
            });
          }}></Banner>
        }}}></HomeStack.Screen>
      <HomeStack.Screen name="Deadline" component={Deadline} options={{header:(scene, previous, navigator)=>{
          return <Banner navigation={scene.navigation} LeftIcon={{name : "LeftArrow", color : "#707070"}} Icon={SearchIcon} profile={require('./src/imgs/profile1.jpg')} pressIn={{color : "#707070", backgroundColor : "#fff"}} pressOut={{color : "#707070", backgroundColor : "#fff"}} press={()=>scene.navigation.navigate("Home")}></Banner>
        }}}></HomeStack.Screen>
        <HomeStack.Screen name="Parameters" component={Parameters} options={{header:(scene, previous, navigator)=>{
          return <Banner navigation={scene.navigation} LeftIcon={{name : "LeftArrow", color : "#707070"}} title={"Paramètres"} pressIn={{color : "#707070", backgroundColor : "#fff"}} pressOut={{color : "#707070", backgroundColor : "#fff"}} press={()=>scene.navigation.navigate("Home")}></Banner>
        }}}></HomeStack.Screen>
        <HomeStack.Screen name="ChoosePathFile" component={ChoosePathFile} options={{header:(scene, previous, navigator)=>{
          return <Banner navigation={scene.navigation} LeftIcon={{name : "LeftArrow", color : "#707070"}} title={"Paramètres"} pressIn={{color : "#707070", backgroundColor : "#fff"}} pressOut={{color : "#707070", backgroundColor : "#fff"}} press={()=>scene.navigation.navigate("Home")}></Banner>
        }}}></HomeStack.Screen>
        <HomeStack.Screen name="CameraWindow" component={CameraWindow} options={{header:(scene, previous, navigator)=>{
          return <Banner navigation={scene.navigation} LeftIcon={{name : "LeftArrow", color : "#707070"}} pressIn={{color : "#707070", backgroundColor : "#fff"}} pressOut={{color : "#707070", backgroundColor : "#fff"}} press={()=>scene.navigation.navigate("Home")}></Banner>
        }}}></HomeStack.Screen>
        <HomeStack.Screen name="AddDeadline" component={AddDeadline} options={{header:(scene, previous, navigator)=>{
          return <Banner navigation={scene.navigation} LeftIcon={{name : "LeftArrow", color : "#707070"}} title={"Paramètres"} pressIn={{color : "#707070", backgroundColor : "#fff"}} pressOut={{color : "#707070", backgroundColor : "#fff"}} press={()=>scene.navigation.navigate("Home")}></Banner>
        }}}></HomeStack.Screen>
    </HomeStack.Navigator>
  )
}

function FormsNavigator(){
  return(
    <FormsStack.Navigator initialRouteName="LoginForm" screenOptions={{ headerShown: "screen" }}>
      <Stack.Screen name="LoginForm" component={LoginForm} options={{header:(scene, previous, navigator)=>{
          return <Banner LeftIcon={{name : "Logo", color : "#000"}} buttonTitle="Créer un compte" handler={()=>scene.navigation.navigate("FormsNavigator", {screen : "SigninForm"})}></Banner>
        }}}></Stack.Screen>
        <Stack.Screen name="NewPassForm" component={NewPassForm} options={{header:(scene, previous, navigator)=>{
          return <Banner LeftIcon={{name : "Logo", color : "#000"}} buttonTitle="Créer un compte" handler={()=>scene.navigation.navigate("FormsNavigator", {screen : "SigninForm"})}></Banner>
        }}}></Stack.Screen>
        <Stack.Screen name="ChangePassForm" component={ChangePassForm} options={{header:(scene, previous, navigator)=>{
          return <Banner LeftIcon={{name : "Logo", color : "#000"}} buttonTitle="Créer un compte" handler={()=>scene.navigation.navigate("FormsNavigator", {screen : "SigninForm"})}></Banner>
        }}}></Stack.Screen>
        <Stack.Screen name="SigninForm" component={SigninForm} options={{header:(scene, previous, navigator)=>{
          return <Banner LeftIcon={{name : "Logo", color : "#000"}} buttonTitle="Se connecter" handler={()=>scene.navigation.navigate("FormsNavigator", {screen : "LoginForm"})}></Banner>
        }}}></Stack.Screen>
    </FormsStack.Navigator>
  )
}

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomeNavigator" screenOptions={{ headerShown: false }}>
        <Stack.Screen name="FormsNavigator" component={FormsNavigator}></Stack.Screen>
        <Stack.Screen name="HomeNavigator" component={HomeNavigator}></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});